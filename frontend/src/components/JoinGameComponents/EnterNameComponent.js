import style from 'styled-components';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { purple, red } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import { useHistory } from "react-router";

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: red[500],
  '&:hover': {
    backgroundColor: red[700],
  },
}));

const Form = style.form`
  display: flex;
  flex-direction: column;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

const EnterNameComponent = (props) => {
  const {setName, onNameChanged} = props;
  return (
    <Form onSubmit={setName}>
      <h1>Enter Your Name</h1>
      <TextField
        id='outlined-basic'
        label='Name'
        variant='outlined'
        inputProps={{maxlength: "10"}}
        onChange={onNameChanged}
        autoFocus
        autoComplete="off"
      />
      <br />
      <ColorButton type="submit" variant='contained'>Play</ColorButton>
    </Form>
  )
}

export default EnterNameComponent
