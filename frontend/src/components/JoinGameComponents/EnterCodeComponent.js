import React from 'react'
import style from 'styled-components';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import {purple, red } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import backjox from '../../media/backjox.png';

const ImageWrapper = style.img`
  width: 100vw;
`

const Form = style.form`
  display: flex;
  flex-direction: column;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: red[500],
  '&:hover': {
    backgroundColor: red[700],
  },
}));

const EnterCodeComponent = (props) => {
  const {onCodeChanged, joinGame, badCode, gameAlreadyStarted} = props;
  const isNotLetter = (charCode) => {
    return !((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode === 8);
  }

  const onKeyPressed = (e) => {
    let charCode = e.charCode;
    // charcode for enter is 13
    if (isNotLetter(charCode) && charCode !== 13) {
      e.preventDefault();
    }
  }


  return (
    <Form onSubmit={joinGame}>
      <ImageWrapper src={backjox} alt="logo" />
      <br />
      <TextField
        id='outlined-basic'
        label='Room Code'
        variant='outlined'
        onKeyPress={onKeyPressed}
        onInput={(e) => e.target.value = ("" + e.target.value).toUpperCase()}
        inputProps={{maxlength: "4", type: "text"}}
        onChange={onCodeChanged}
        autoFocus
        autoComplete="off"
      />
      <br />
      <ColorButton type="submit" variant='contained'>Join</ColorButton>
      { badCode && <h2>Bad Code</h2>}
      { gameAlreadyStarted && <h2>Game Already Started</h2>}
    </Form>
  )
}

export default EnterCodeComponent
