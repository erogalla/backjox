import React, { useEffect, useRef, useState } from 'react';
import canvasImage from '../media/canvas.png';

const Canvas = (props) => {
  const { onDraw, color, lines } = props;

  const canvasRef = useRef(null);
  const contextRef = useRef(null);
  const [drawing, setDrawing] = useState(false);
  const [lastCoordinate, setLastCoordinates] = useState([]);


  useEffect(() => {
    const canvas = canvasRef.current;

    // Set initial canvas context
    const context = canvas.getContext('2d');
    context.lineCap = 'round';
    context.lineWidth = Math.floor(canvas.width / 40);
    contextRef.current = context;

    // Set background Image
    const background = new Image();
    background.src = canvasImage;

    background.onload = () => {
      context.drawImage(background, 0, 0, canvas.width, canvas.height);
      lines && lines.forEach((lineData, index) => {
        const lineColor = lineData[0];
        const startCoordinate = lineData[1];
        const coordinates = lineData.splice(2);
        if (coordinates.length <= 0) return;


        context.strokeStyle = lineColor;
        context.fillStyle = lineColor;
        context.beginPath();
        context.moveTo(
          (startCoordinate[0] * canvas.width) / 1000,
          (startCoordinate[1] * canvas.height) / 800
        );

        coordinates.forEach((coordinate) => {
          context.lineTo(
            (coordinate[0] * canvas.width) / 1000,
            (coordinate[1] * canvas.height) / 800
          );
        });

        context.stroke();
        context.closePath();
      });

      context.strokeStyle = color;
      context.fillStyle = color;
    };

    window.addEventListener('mousedown', startDrawing);
    window.addEventListener('mouseup', stopDrawing);
    window.addEventListener('touchstart', startDrawing);
    window.addEventListener('touchend', stopDrawing);

    return () => {
      window.removeEventListener('mousedown', startDrawing);
      window.removeEventListener('mouseup', stopDrawing);
      window.removeEventListener('touchstart', startDrawing);
      window.removeEventListener('touchend', stopDrawing);
    };
  }, []);

  const maxWidthHeight = () => {
    const w = window.innerWidth;
    const h = Math.floor(window.innerHeight * 0.9);

    const boundedByWidth = (w * 5) / 4 < h;

    if (boundedByWidth) {
      return [w, (w * 5) / 4];
    }
    return [(h * 4) / 5, h];
  };

  const startDrawing = (e) => {
    console.log(e)
    const x = e.clientX - canvasRef.current.offsetLeft;
    const y = e.clientY - canvasRef.current.offsetTop;
    contextRef.current.beginPath();
    contextRef.current.moveTo(x, y);
    setDrawing(true);
  };

  const stopDrawing = () => {
    setDrawing(false);
    contextRef.current.closePath();
    onDraw && onDraw(0, 0, true);
  };

  const draw = (e) => {
    e.preventDefault();
    const {nativeEvent} = e;
    console.log(nativeEvent)
    if (!drawing) {
      return;
    }
    let { offsetX, offsetY } = nativeEvent;

    if (offsetX === undefined || offsetY === undefined) {
      offsetX = nativeEvent.touches[0].clientX - canvasRef.current.offsetLeft;
      offsetY = nativeEvent.touches[0].clientY - canvasRef.current.offsetTop;
    }

    contextRef.current.lineTo(offsetX, offsetY);
    contextRef.current.stroke();
    if (lastCoordinate !== []) {
      const a = offsetX - lastCoordinate[0];
      const b = offsetY - lastCoordinate[1];
      const distance = Math.sqrt(a * a + b * b);
      const normalizedDistance = (distance * 1000) / canvasRef.current.width;
      if (normalizedDistance > 1) {
        onDraw &&
          onDraw(
            Math.floor((offsetX * 1000) / canvasRef.current.width),
            Math.floor((offsetY * 800) / canvasRef.current.height),
            false
          );
      }
    }

    setLastCoordinates([offsetX, offsetY]);
  };

  return (
    <canvas
      ref={canvasRef}
      width={maxWidthHeight()[0]}
      height={maxWidthHeight()[1]}
      onMouseMove={draw}
      onTouchMove={draw}
    />
  );
};

export default Canvas;
