import React, { useState, useEffect,  useContext } from 'react';
import { SocketContext } from '../socket'
import styled from 'styled-components';
import openSocket, { io } from 'socket.io-client';


const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 50px;
`;

const Form = styled.form`
  text-align: right;
  label {
    margin-right: 10px;
  }
`;

const RecievedAnswer = styled.form`
`;



function TruthsInputRoute() {

  const socket = useContext(SocketContext)

  const [truth1, setTruth1] = useState('');
  const [truth2, setTruth2] = useState('');
  const [lie, setLie] = useState('');

  const [option1, setOption1] = useState('')
  const [option2, setOption2] = useState('')
  const [option3, setOption3] = useState('')

  const [answers, setAnswers] = useState([])

  const submitForm = (e) => {
    e.preventDefault();
    console.log(truth1, truth2, lie)
    socket.emit('send answer', {truth1, truth2, lie})
  }

  const handleNewAnswer = answer => {
    console.log('Recieved new answer', answer)
    
    // If we are using the first 
    if (answers.length == 0) {
      console.log('updating first')
      const start = Math.floor(Math.random() * 3)
      const choices = Object.values(answer)
      
      setOption1(choices[start])
      setOption2(choices[(start + 1) % 3])
      setOption3(choices[(start + 2) % 3])
      
    }

    setAnswers(arr => [...arr, answer])
  }

  useEffect(() => {
    socket.on('recieve answer', handleNewAnswer)
  })
  
  return (
    <MainWrapper>
      <div style={{width: 'fit-content', margin: '2rem auto'}}>
        <Form>

          <label for="truth1">First Truth:</label>
          <input type="text" id="truth1" name="truth1" onChange={e => setTruth1(e.target.value)} /><br></br>

          <label for="truth2">Second Truth:</label>
          <input type="text" id="truth2" name="truth2" onChange={e => setTruth2(e.target.value)}/><br></br>

          <label for="lie">Lie:</label>
          <input type="text" id="lie" name="lie" onChange={e => setLie(e.target.value)}/><br></br>

          <input type="submit" value="Submit" onClick={submitForm} />

        </Form>
        {answers.length > 0 && 
          <Form>
            <label>okay</label>
            <input type="radio" />
          </Form> 
        }
      </div>
    </MainWrapper>
  );
}

export default TruthsInputRoute;
