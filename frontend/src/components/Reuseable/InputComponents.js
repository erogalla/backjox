import React from 'react'
import Button from '@mui/material/Button';
import {purple, red } from '@mui/material/colors';
import { styled } from '@mui/material/styles';

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(purple[500]),
  backgroundColor: red[500],
  '&:hover': {
    backgroundColor: red[700],
  },
}));

export const RedButton = (props) => {
  return (
    <ColorButton {...props}>{props.children}</ColorButton>
  )
}