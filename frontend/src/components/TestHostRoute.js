import React, { useState, useEffect,  useContext } from 'react';
import { SocketContext } from '../socket'
import styled from 'styled-components';
import openSocket, { io } from 'socket.io-client';


function TestHostRoute() {
  const socket = useContext(SocketContext)
  const [code, setCode] = useState('');
  const [names, setNames] = useState([]);

  // const [responseRoundStarted, setResponseRoundStarted] = useState(false);
  // const [responses, setResponses] = useState({});

  
  useEffect(() => {
    // hosts
    socket.on('codeCreated', (code) => setCode(code));
    socket.on('userJoined', (newName) => setNames((names) => ([...names, newName])));
    socket.on('responseRoundOver', () => console.log('response Round Over'));
    socket.on('votingRoundOver', (truth1, truth1Percentage, truth2, truth2Percentage, lie, liePercentage) => {
      console.log('truth1', truth1, truth1Percentage);
      console.log('truth2', truth2, truth2Percentage);
      console.log('lie', lie, liePercentage);
    })
  }, []);

  const host = () => {
    socket.emit('hostGame', 'twoTruthsOneLie'); 
  }

  const startResponseRound = () => {
    socket.emit('startResponseRound', 60); 
  }
  
  const startVotingRound = () => {
    socket.emit('startVotingRound', 10);
  }

  const endGame = () => {
    socket.emit('endGame');
  }
  

  return (
    <>
      <button onClick={host}>Host</button>

      <h1>Code: {code}</h1>
      <p>
        Joined Players: {names.map((name) => `${name}, `)}
      </p>
      <button onClick={startResponseRound}>startResponseRound</button>
      <button onClick={startVotingRound}>startVotingRound</button>
      <button onClick={endGame}>endGame</button>
      {/* {
        responseRoundStarted && (
          <>
            Response Round Started!
            Recieved Responses: {Object.keys(responses).length}
          </>
        )
      } */}
    </>
  );
}

export default TestHostRoute;
