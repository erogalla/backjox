import React, { useState, useEffect,  useContext } from 'react';
import style from 'styled-components';
// import backjox from '../media/backjox.png';
import EnterCodeComponent from "./JoinGameComponents/EnterCodeComponent";
import { SocketContext } from '../socket'
import TwoTruthsOneLie from './TwoTruthsOneLie';
import ImpostorArtist from './ImpostorArtist';

const MainWrapper = style.div`
  display: flex;
  flex-direction: column;
  height: 90vh;
  align-items: center;
  justify-content: center;
`;

function HomeRoute() {
  const leader = 0
  const joinedGame = 1;
  const badCode = 2;
  const gameAlreadyStarted = 3;
  const gameFull = 4;

  
  const [code, setCode] = useState('');
  const [gameState, setGameState] = useState(Array(5).fill(false));
  const [gameType, setGameType] = useState('');

  const socket = useContext(SocketContext)

  const setState = (index, newValue) => {
    setGameState((gameState) => ([...gameState.slice(0, index), newValue, ...gameState.splice(index + 1)]));
  }

  useEffect(() => {
    // Game Setup
    socket.on('gameDetails', gameDetails => {
      console.log('gameDetails', gameDetails);
      setState(joinedGame, true);
      setState(leader, gameDetails.isLeader);
      setGameType(gameDetails.gameType);
    });
    socket.on('badCode', () => setState(badCode, true));
    socket.on('gameAlreadyStarted', () => setState(gameAlreadyStarted, true));
    socket.on('gameFull', () => setState(gameFull, true));

  }, [socket, setCode]);

  const onCodeChanged = (e) => {
    e.preventDefault();
    setCode(e.target.value);
  }

  const joinGame = (e) => {
    e.preventDefault();
    setState(badCode, false);
    setState(gameAlreadyStarted, false);
    socket.emit('joinGame', code.toLowerCase());
  }

  // returns true if index is true and all indicies after index are false;
  const only = (index) => {
    for (let i = index + 1; i < gameState.length; i++) {
      if (gameState[i]) {
        return false;
      }
    }

    return gameState[index];
  }

  return (
    <MainWrapper>
      { /* Set Code for Game */
        !gameState[joinedGame] && 
        <EnterCodeComponent 
          onCodeChanged={onCodeChanged} 
          joinGame={joinGame} 
          badCode={gameState[badCode]} 
          gameAlreadyStarted={gameState[gameAlreadyStarted]} 
          gameFull={gameState[gameFull]}
        /> 
      }
      
      { /* When joined the game, display game */
        only(joinedGame) && 
        (
          ( gameType === 'twoTruthsOneLie' &&
            <TwoTruthsOneLie leader={gameState[leader]} />
          ) ||
          ( gameType === 'impostorArtist' &&
            <ImpostorArtist leader={gameState[leader]} />
          )
        )
      }

    </MainWrapper>
  );
}

export default HomeRoute;
