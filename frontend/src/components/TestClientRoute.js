import React, { useState, useEffect,  useContext } from 'react';
import { SocketContext } from '../socket'
import styled from 'styled-components';
import openSocket, { io } from 'socket.io-client';


function TestClientRoute() {
  const [code, setCode] = useState('');
  const [name, setName] = useState('');
  const [leader, setLeader] = useState(false);

  const [responseRoundStarted, setResponseRoundStarted] = useState(false);
  const [responseRoundEnded, setResponseRoundEnded] = useState(false);
  const [response, setResponse] = useState({});

  const [votingRoundStarted, setVotingRoundStarted] = useState(false);
  const [votingRound, setVotingRound] = useState(0);
  const [options, setOptions] = useState([]);
  const [checked, setChecked] = useState(0);

  const socket = useContext(SocketContext)

  console.log(code);

  useEffect(() => {
    // client
    socket.on('gameDetails', gameDetails => {
      console.log('gameDetails', gameDetails);
      setLeader(gameDetails.isLeader);
    });
    socket.on('responseRoundStarted', (roundTime => {
      console.log('responseRoundStarted', roundTime);
      setResponseRoundStarted(true);
    }));
    socket.on('responseRoundOver', () => setResponseRoundEnded(true));
    socket.on('responseRoundStarted', (roundTime => {
      console.log('responseRoundStarted', roundTime);
      setResponseRoundStarted(true);
    }));
    socket.on('votingRoundStarted', (round, roundTime, votingOptions) => {
      console.log('votingRoundStarted', round, roundTime, votingOptions);
      setVotingRound(round);
      setVotingRoundStarted(true);
      setOptions(votingOptions);
    });
    socket.on('votingRoundOver', () => setVotingRoundStarted(false));
    socket.on('hostDisconnected', () => console.log('Host Disconnected'));

    socket.on('badCode', () => console.log('bad code'));

  }, [socket, setCode]);

  const join = (e) => {
    e.preventDefault();
    socket.emit('joinGame', code);
  }

  const serverSetName = (e) => {
    e.preventDefault();
    socket.emit('setName', name);
    console.log('called set name', name);
  }

  const startGame = () => {
    socket.emit('startGame');
  }

  const submitResponse = (e) => {
    e.preventDefault();
    socket.emit('submitResponse', response);
  }

  const vote = (e) => {
    e.preventDefault();
    console.log(options[checked], votingRound);
    socket.emit('vote', options[checked], votingRound);
  }
  

  return (
    <>
      <form>
        <input type="text" id="code" name="code" onChange={e => setCode(e.target.value)} />
        <button onClick={join}>Join</button>
      </form>

      <form>
        <input type="text" id="name" name="name" onChange={e => setName(e.target.value)} />
        <button onClick={serverSetName}>Set Name</button>
      </form>
      {leader && <button onClick={startGame}>StartGame</button> }
      {
        responseRoundStarted && !responseRoundEnded && (
          <>
            <p>Response Round Started!</p>
            <form>
              <input type="text" id="Truth1" name="Truth1" onChange={e => setResponse((response) => ({...response, truth1: e.target.value}))} />
              <input type="text" id="Truth2" name="Truth2" onChange={e => setResponse((response) => ({...response, truth2: e.target.value}))} />
              <input type="text" id="Lie" name="Lie" onChange={e => setResponse((response) => ({...response, lie: e.target.value}))} />
              <button onClick={submitResponse}>Submit Response</button>
            </form>
          </>
        )
      }
      {
        votingRoundStarted && (
          <>
            <p>Voting Round Started!</p>
            <form>
              <input type="radio" id="Option0" name="Option0" onChange={() => setChecked(0)} checked={checked === 0} />
              {options[0]}
              <input type="radio" id="Option1" name="Option1" onChange={() => setChecked(1)} checked={checked === 1} />
              {options[1]}
              <input type="radio" id="Option2" name="Option2" onChange={() => setChecked(2)} checked={checked === 2} />
              {options[2]}
              <button onClick={vote}>Vote</button>
            </form>
          </>
        )
      }
    </>
  );
}

export default TestClientRoute;
