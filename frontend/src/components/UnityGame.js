import React, { useState, useEffect, Fragment } from 'react';
import Unity, { UnityContext } from 'react-unity-webgl';
import styled from 'styled-components';

const unityContext = new UnityContext({
  productName: 'BackJox Games',
  companyName: 'BackJox',
  // The url's of the Unity WebGL runtime, these paths are public and should be
  // accessible from the internet and relative to the index.html.
  loaderUrl: 'BackJoxWebBuild/Build/BackJoxWebBuild.loader.js',
  dataUrl: 'BackJoxWebBuild/Build/BackJoxWebBuild.data',
  frameworkUrl: 'BackJoxWebBuild/Build/BackJoxWebBuild.framework.js',
  codeUrl: 'BackJoxWebBuild/Build/BackJoxWebBuild.wasm',
  streamingAssetsUrl: 'BackJoxWebBuild/Build/streamingassets',
  // Additional configuration options.
  webglContextAttributes: {
    preserveDrawingBuffer: true,
  },
});

const Wrapper = styled.div`
  height: 100%;
  text-align: center;
  margin: 0 auto;
`;

const UnityContainer = styled.div`
  height: 100%;
  background-color: 'white';
`;

const LoadingOverlay = styled.div`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
  background-color: rgb(51, 48, 48);
  display: flex;
  justify-content: center;
  align-items: center;
`;

const ProgressBar = styled.div`
  width: 100px;
  height: 10px;
  background-color: rgb(99, 99, 99);
  border-radius: 5px;
  overflow: hidden;
`;

const ProgressBarFill = styled.div`
  height: 10px;
  background-color: rgb(190, 190, 190);
  transition: width 0.5s ease;
`;

const UnityCanvas = styled(Unity)`
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 100%;
`;

const UnityGame = () => {
  const [isUnityMounted, setIsUnityMounted] = useState(true);
  const [isLoaded, setIsLoaded] = useState(false);
  const [progression, setProgression] = useState(0);

  // When the component is mounted, we'll register some event listener.
  useEffect(() => {
    unityContext.on('canvas', handleOnUnityCanvas);
    unityContext.on('progress', handleOnUnityProgress);
    unityContext.on('loaded', handleOnUnityLoaded);
    return function () {
      unityContext.removeAllEventListeners();
    };
  }, []);

  // Built-in event invoked when the Unity canvas is ready to be interacted with.
  function handleOnUnityCanvas(canvas) {
    canvas.setAttribute('role', 'unityCanvas');
  }

  // Built-in event invoked when the Unity app's progress has changed.
  function handleOnUnityProgress(progression) {
    setProgression(progression);
  }

  // Built-in event invoked when the Unity app is loaded.
  function handleOnUnityLoaded() {
    setIsLoaded(true);
  }

  // Event invoked when the user clicks the button, the unity container will be
  // mounted or unmounted depending on the current mounting state.
  function handleOnClickUnMountUnity() {
    if (isLoaded === true) {
      setIsLoaded(false);
    }
    setIsUnityMounted(isUnityMounted === false);
  }

  return (
    <Fragment>
      <Wrapper>
        {/* The Unity container */}
        {isUnityMounted === true && (
          <Fragment>
            <UnityContainer>
              <iframe
                title="Unity"
                id='webgl_iframe'
                frameborder='0'
                allow='autoplay; fullscreen; vr'
                allowfullscreen=''
                allowvr=''
                mozallowfullscreen='true'
                src='https://play.unity3dusercontent.com/webgl/ef25e550-e578-4834-bb6a-de2edb88bbd4?screenshot=false&embedType=embed'
                width='810'
                height='640'
                onmousewheel=''
                webkitallowfullscreen='true'
              ></iframe>
            </UnityContainer>
          </Fragment>
        )}
      </Wrapper>
    </Fragment>
  );
};

export default UnityGame;
