import React, { useState, useEffect,  useContext } from 'react';
import { SocketContext } from '../socket'
import styled from 'styled-components';
import openSocket, { io } from 'socket.io-client';


function TestHostRoute2() {
  const socket = useContext(SocketContext)
  const [code, setCode] = useState('');
  const [names, setNames] = useState([]);
  const [allDrawingRoundsOver, setAllDrawingRoundsOver]  = useState(false);

  
  useEffect(() => {
    // hosts
    socket.on('codeCreated', (code) => setCode(code));
    socket.on('userJoined', (newName) => setNames((names) => ([...names, newName])));
    socket.on('drawingRoundOver', () => console.log('drawingRoundOver'));
    socket.on('allDrawingRoundsOver', () => {
      console.log('allDrawingRoundsOver');
      setAllDrawingRoundsOver(true);
    });

    socket.on('responseRoundOver', () => console.log('response Round Over'));
    socket.on('votingRoundOver', (truth1, truth1Percentage, truth2, truth2Percentage, lie, liePercentage) => {
      console.log('truth1', truth1, truth1Percentage);
      console.log('truth2', truth2, truth2Percentage);
      console.log('lie', lie, liePercentage);
    })
  }, []);

  const host = () => {
    socket.emit('hostGame', 'impostorArtist'); 
  }

  const displaySecret = () => {
    socket.emit('displaySecret', 5);
  }

  const startDrawingRound = () => {
    socket.emit('startDrawingRound', 5);
  }
  
  const startVotingRound = () => {
    socket.emit('startVotingRound', 10);
  }

  const endGame = () => {
    socket.emit('endGame');
  }
  

  return (
    <>
      <button onClick={host}>Host</button>

      <h1>Code: {code}</h1>
      <p>
        Joined Players: {names.map((name) => `${name}, `)}
      </p>
      <button onClick={displaySecret}>Display Secret</button>
      <button onClick={startDrawingRound}>Start Drawing Round</button>
      { allDrawingRoundsOver && <p>All Drawing Rounds Over</p> }
      <button onClick={startVotingRound}>Start Voting Round</button>
    </>
  );
}

export default TestHostRoute2;
