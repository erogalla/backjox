import React, {useState, useEffect} from 'react';
import styled from 'styled-components';

const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 50px;
`;

const Form = styled.form`
  text-align: right;
  label {
    margin-right: 10px;
  }
;`


function OutputRoute() {

  const [chosenOption, setChosenOption] = useState(0);

  const [option0, setOption0] = useState('');
  const [option1, setOption1] = useState('');
  const [option2, setOption2] = useState('');

  const submitForm = (e) => {
    e.preventDefault();
    console.log(`user chose option ${chosenOption}`)
  }

  useEffect(() => {
    
  }, []);
  
  return (
    <MainWrapper>
      <div style={{width: 'fit-content', margin: '2rem auto'}}>
        <Form>
          <h2>Select the Lie</h2>
          <label for="truth1">First Truth:</label>
          <input type="radio" id="truth1" name="options" onChange={e => setChosenOption(0)} /><br></br>

          <label for="truth2">Second Truth:</label>
          <input type="radio" id="truth2" name="options" onChange={e => setChosenOption(1)}/><br></br>

          <label for="lie">Lie:</label>
          <input type="radio" id="lie" name="options" onChange={e => setChosenOption(2)}/><br></br>

          <input type="submit" value="Submit" onClick={submitForm} />

        </Form>
      </div>
    </MainWrapper>
  );
}

export default OutputRoute;
