import React, { useState, useEffect,  useContext } from 'react';
import style from 'styled-components';
import { LinearProgress } from '@mui/material';
import { grey, purple, red } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { useHistory } from "react-router";
// import backjox from '../media/backjox.png';
import EnterCodeComponent from "./JoinGameComponents/EnterCodeComponent";
import EnterNameComponent from "./JoinGameComponents/EnterNameComponent";
import { SocketContext } from '../socket'
import { RedButton } from "./Reuseable/InputComponents";
import { Radio, RadioGroup } from "@mui/material";
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import Canvas from "./Canvas";

const MainWrapper = style.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

const LieInputForm = style.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  div {
    margin: 3px;
  }
`;

const CHARACTER_LIMIT = 50;
const INK_LEFT = 200;

function ImpostorArtist({leader}) {
  const startedGame = 0;
  const nameSubmitted = 1;
  const impostor = 2;
  const secretDisplayed = 3; 
  const secretHidden = 4;
  const roundsStarted = 5;
  const drawingRoundStarted = 6;
  const drawingRoundEnded = 7;
  const allDrawingRoundsOver = 8;
  const votingRoundStarted = 9;
  const voteSent = 10;
  const votingRoundEnded = 11;
  const hostDisconnected = 12;
  const gameOver = 13;

  const [gameState, setGameState] = useState(Array(14).fill(false));

  const [name, setName] = useState('');
  const [secretWord, setSecretWord] = useState('');
  const [timesDrawn, setTimesDrawn] = useState(0);
  const [inkLeft, setInkLeft] = useState(INK_LEFT);
  const [color, setColor] = useState('');
  const [lines, setLines] = useState([])

  const [impostorOptions, setImpostorOptions] = useState([]);
  const [checked, setChecked] = useState(0);

  const socket = useContext(SocketContext)

  const setState = (index, newValue) => {
    setGameState((gameState) => ([...gameState.slice(0, index), newValue, ...gameState.splice(index + 1)]));
  }

  useEffect(() => {
    socket.on('secretWord', (secretWord) => {
      setSecretWord(secretWord)
      setState(secretDisplayed, true);
    });
    socket.on('impostor', () => {
      console.log('impostor');
      setState(impostor, true);
      setState(secretDisplayed, true);
    });
    socket.on('hideSecret', () => setState(secretHidden, true));

    socket.on('roundsStarted', () => setState(roundsStarted, true));
    socket.on('drawingRoundStarted', (color, lines) => {
      setInkLeft(INK_LEFT);      
      setLines(lines);
      setColor(color);
      setState(drawingRoundStarted, true);
      setState(drawingRoundEnded, false)
    });
    socket.on('drawingRoundOver', () => {
      setState(drawingRoundEnded, true);
      setTimesDrawn(timesDrawn + 1)
    });
    socket.on('allDrawingRoundsOver', () => setState(allDrawingRoundsOver, true));

    socket.on('votingRoundStarted', (impostorOptions) => {
      setImpostorOptions(impostorOptions);
      setState(votingRoundStarted, true);
      setState(voteSent, false);
      setState(votingRoundEnded, false);
    });
    socket.on('votingRoundOver', () => setState(votingRoundEnded, true));

    socket.on('gameOver', () => setState(gameOver, true));
    socket.on('votedImpostor', () => setState(gameOver, true));
    socket.on('votedWrong', () => setState(gameOver, true));
    socket.on('hostDisconnected', () => setState(hostDisconnected, true));

  }, [socket]);
  
  const onNameChanged = (e) => {
    e.preventDefault();
    setName(e.target.value);
  }

  const serverSetName = (e) => {
    e.preventDefault();
    setState(nameSubmitted, true);
    socket.emit('setName', name);
    console.log('called set name', name);
  }

  const startGame = () => {
    socket.emit('startGame');
    setState(startedGame, true);
  }

  const draw = (x, y, endDrawing) => {
    // if (inkLeft <= 0) socket.emit('endDrawingRound');

    setInkLeft((inkLeft) => inkLeft - 1);
    socket.emit('draw', inkLeft - 1, x, y, endDrawing);
  }

  const vote = (e) => {
    e.preventDefault();
    setState(voteSent, true);
    if (impostorOptions[checked])
      socket.emit('vote', impostorOptions[checked].id);
  }
  

  const resetGame = () => {
    window.location.reload(false);
  }

  // returns true if index is true and all indicies after index are false;
  const only = (index) => {
    for (let i = index + 1; i < gameState.length; i++) {
      if (gameState[i]) {
        return false;
      }
    }

    return gameState[index];
  }

  return (
    <MainWrapper>
      
      { /* Set Name for Game */
        !gameState.includes(true) &&
        <EnterNameComponent onNameChanged={onNameChanged} setName={serverSetName}/>
      }

      { /* Game is joined and name is submitted */
        only(nameSubmitted) && (
          <>
            { /* Let Leader Start the game */
              leader && !gameState[startedGame]
              ? <RedButton onClick={startGame}>Start Game</RedButton> 
              : <h3>
                  Waiting for the game to start...
                </h3>
            }
          </>
        )
      }

      { /* Secret is displayed */
        only(secretDisplayed) && (
          <>
            <h3>The secret word is...</h3>
            {
              gameState[impostor] 
              ? <p>??? You're the impostor.</p>
              : <p>{secretWord}</p>
            }
          </>
        )
      }

      { /* Rounds have started */
        only(secretHidden) && (
          <>
            <h3>
              Waiting for the game to continue...
            </h3>
          </>
        )
      }

      { /* Rounds have started */
        only(roundsStarted) && (
          <>
            <h3>
              Waiting for other players...
            </h3>
          </>
        )
      }

      { /* Rounds have started */
        only(drawingRoundStarted) && (
          <>
            
            <Canvas onDraw={draw} color={color} lines={lines}/>
            <h3>
              Please draw the secret word
            </h3>
            {console.log(inkLeft)}
            <Box sx={{ width: '100%' }}>
              <LinearProgress variant="determinate" value={(inkLeft / 2)} />
            </Box>
          </>
        )
      }

      { /* Player is drawing */
        only(drawingRoundEnded) && (
          <>
            <h3>
              {
                timesDrawn < 2 
                ? 'Please wait your turn...'
                : 'Waiting for the game to continue...'
              }
            </h3>
          </>
        )
      }

      { /* Rounds have started */
        only(allDrawingRoundsOver) && (
          <>
            <h3>
              Waiting for the game to continue...
            </h3>
          </>
        )
      }

      { /* Once the Voting round has started */
        only(votingRoundStarted) && (
          <>
            <FormControl component="fieldset">
              <FormLabel component="legend">Who's the Impostor?</FormLabel>the
              <RadioGroup
                aria-label="two truths and one lie"
                defaultValue="option1"
                name="radio-buttons-group"
              >
                { /* TODO: Add colors to labels */  }
                {
                  impostorOptions.map((impostorData, index) => (
                    <
                      FormControlLabel 
                      value={`option${index}`} 
                      control={
                        <Radio onChange={() => setChecked(index)
                      } 
                      checked={checked === index}/>} 
                      label={impostorData.name ?? ""} 
                    />
                  ))
                }
              </RadioGroup>
            </FormControl>
            <RedButton onClick={vote}>Vote</RedButton>
          </>
        )
      }

      { /* Vote Sent */
        only(voteSent) && (
          <>
            <h3>Vote Received</h3>
            <h4><i>{impostorOptions[checked].name}</i></h4>
          </>
        )
      }

      {
        /* Voting round is over */
        only(votingRoundEnded) && (
          <>
            <h3>
              Waiting for the game to continue...
            </h3>
          </>
        )
      }

      { // Host Disconnected
        only(hostDisconnected) && (
          <>  
            <h3>Host Disconnected</h3>
            <RedButton onClick={resetGame}>Play Again</RedButton>
          </>
        )
      }

      { // Game Over
        only(gameOver) && (
          <>  
            <h3>Thanks for Playing!</h3>
            <RedButton onClick={resetGame}>Play Again</RedButton>
          </>
        )
      }
    </MainWrapper>
  );
}

export default ImpostorArtist;
