import React, { useState, useEffect,  useContext } from 'react';
import style from 'styled-components';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import { grey, purple, red } from '@mui/material/colors';
import { styled } from '@mui/material/styles';
import { useHistory } from "react-router";
// import backjox from '../media/backjox.png';
import EnterCodeComponent from "./JoinGameComponents/EnterCodeComponent";
import EnterNameComponent from "./JoinGameComponents/EnterNameComponent";
import { SocketContext } from '../socket'
import { RedButton } from "./Reuseable/InputComponents";
import { Radio, RadioGroup } from "@mui/material";
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

const MainWrapper = style.div`
  display: flex;
  flex-direction: column;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

const LieInputForm = style.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  div {
    margin: 3px;
  }
`;

const VoteInputForm = style.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  div {
    margin: 3px;
  }
`;

const CHARACTER_LIMIT = 50;

function TwoTruthsOneLie({leader}) {
  const startedGame = 0;
  const nameSubmitted = 1;
  const allResponseReceived = 2;
  const responseRoundStarted = 3;
  const responseSubmitted = 4;
  const responseRoundEnded = 5;
  const votingRoundStarted = 6;
  const voteSent = 7;
  const votingRoundEnded = 8;
  const hostDisconnected = 9;
  const gameOver = 10;

  const [gameState, setGameState] = useState(Array(11).fill(false));

  const [name, setName] = useState('');
  const [response, setResponse] = useState({});

  const [votingRound, setVotingRound] = useState(0);
  const [options, setOptions] = useState([]);
  const [checked, setChecked] = useState(0);

  const socket = useContext(SocketContext)

  const setState = (index, newValue) => {
    setGameState((gameState) => ([...gameState.slice(0, index), newValue, ...gameState.splice(index + 1)]));
  }

  useEffect(() => {
    // Response Round
    socket.on('responseRoundStarted', (roundTime => {
      console.log('responseRoundStarted', roundTime);
      setState(responseRoundStarted, true);
    }));
    socket.on('responseRoundOver', () => setState(responseRoundEnded, true));

    // Voting Round
    socket.on('votingRoundStarted', (round, roundTime, name, option1, option2, option3) => {
      console.log('votingRoundStarted', round, roundTime, name, option1, option2, option3);
      setVotingRound(round);
      setOptions([option1, option2, option3]);
      setChecked(0);
      setState(votingRoundStarted, true);
      setState(voteSent, false);
      setState(votingRoundEnded, false);
    });
    socket.on('votingRoundOver', () => {
      setState(votingRoundEnded, true);
    });

    // Game over
    socket.on('gameOver', () => setState(gameOver, true));

    // etc
    socket.on('hostDisconnected', () => setState(hostDisconnected, true));

  }, [socket]);
  
  const onNameChanged = (e) => {
    e.preventDefault();
    setName(e.target.value);
  }

  const startGame = () => {
    socket.emit('startGame');
    setState(startedGame, true);
  }

  const submitResponse = (e) => {
    e.preventDefault();
    setState(responseSubmitted, true);
    socket.emit('submitResponse', response);
  }

  const vote = (e) => {
    e.preventDefault();
    setState(voteSent, true);
    console.log(options[checked], votingRound);
    socket.emit('vote', options[checked], votingRound);
  }
  
  const serverSetName = (e) => {
    e.preventDefault();
    setState(nameSubmitted, true);
    socket.emit('setName', name);
    console.log('called set name', name);
  }
  // e => setResponse((response) => ({...response, lie: e.target.value}))
  const onLieInputChange = (e, target) => {
    e.preventDefault();
    setResponse((response) => {
      response[target] = e.target.value;
      return response;
    });
    
    setState(allResponseReceived, ("lie" in response) && response["lie"] !== "" &&
    ("truth1" in response) && response["truth1"] !== "" && ("truth2" in response) && response["truth2"] !== "");
  }

  const resetGame = () => {
    window.location.reload(false);
  }

  // returns true if index is true and all indicies after index are false;
  const only = (index) => {
    for (let i = index + 1; i < gameState.length; i++) {
      if (gameState[i]) {
        return false;
      }
    }

    return gameState[index];
  }

  return (
    <MainWrapper>
      
      { /* Set Name for Game */
        !gameState.includes(true) &&
        <EnterNameComponent onNameChanged={onNameChanged} setName={serverSetName}/>
      }

      { /* Game is joined and name is submitted */
        only(nameSubmitted) && (
          <>
            { /* Let Leader Start the game */
              leader && !gameState[startedGame]
              ? <RedButton onClick={startGame}>Start Game</RedButton> 
              : <h3>
                  Waiting for the game to start...
                </h3>
            }
          </>
        )
      }

      { /* Response Round */
        only(responseRoundStarted) && (
          <>
            <p>Response Round Started!</p>
            <LieInputForm onSubmit={submitResponse}>
              <TextField
                id='outlined-basic'
                label='First Truth'
                variant='outlined'
                inputProps={{maxlength: CHARACTER_LIMIT}}
                onChange={e => onLieInputChange(e, "truth1")}
                autoFocus
                autoComplete="off"
              />
              <TextField
                id='outlined-basic'
                label='Second Truth'
                variant='outlined'
                inputProps={{maxlength: CHARACTER_LIMIT}}
                onChange={e => onLieInputChange(e, "truth2")}
                autoComplete="off"
              />
              <TextField
                id='outlined-basic'
                label='Lie'
                variant='outlined'
                inputProps={{maxlength: CHARACTER_LIMIT}}
                onChange={e => onLieInputChange(e, "lie")}
                autoComplete="off"
              />
              <RedButton type="submit" disabled={!gameState[allResponseReceived]}>Submit Response</RedButton>
            </LieInputForm>
          </>
        )
      }

      {
        only(responseSubmitted) && (
          <>
            <h3>
              Waiting for other players...
            </h3>
          </>
        )
      }

      { /* Response round is over and we're waiting for voting to start. */
        only(responseRoundEnded) && (
          <>
            <h3>
              Waiting for the game to continue...
            </h3>
          </>
        )
      }

      { /* Voting Round */
        only(votingRoundStarted) && (
          <>
            <FormControl component="fieldset">
              <FormLabel component="legend">Which is the lie?</FormLabel>
              <RadioGroup
                aria-label="two truths and one lie"
                defaultValue="option1"
                name="radio-buttons-group"
              >
                <FormControlLabel value="option1" control={<Radio onChange={() => setChecked(0)} checked={checked === 0}/>} label={options[0] ?? ""} />
                <FormControlLabel value="option2" control={<Radio onChange={() => setChecked(1)} checked={checked === 1}/>} label={options[1] ?? ""} />
                <FormControlLabel value="option3" control={<Radio onChange={() => setChecked(2)} checked={checked === 2}/>} label={options[2] ?? ""} />
              </RadioGroup>
            </FormControl>
            <RedButton onClick={vote}>Vote</RedButton>
          </>
        )
      }

      { /* Vote Sent */
        only(voteSent) && (
          <>
            <h3>Vote Received</h3>
            <h4><i>{options[checked]}</i></h4>
          </>
        )
      }

      {
        /* Voting round is over */
        only(votingRoundEnded) && (
          <>
            <h3>
              Waiting for the game to continue...
            </h3>
          </>
        )
      }

      { // Host Disconnected
        only(hostDisconnected) && (
          <>  
            <h3>Host Disconnected</h3>
            <RedButton onClick={resetGame}>Play Again</RedButton>
          </>
        )
      }

      { // Game Over
        only(gameOver) && (
          <>  
            <h3>Thanks for Playing!</h3>
            <RedButton onClick={resetGame}>Play Again</RedButton>
          </>
        )
      }
    </MainWrapper>
  );
}

export default TwoTruthsOneLie;
