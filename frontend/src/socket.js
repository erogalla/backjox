import io from 'socket.io-client';
import React from 'react';

const URL = process.env.NODE_ENV === 'production' 
            ? 'https://backjox.herokuapp.com/' 
            : 'http://localhost:5000'; 

export const socket = io(URL);
export const SocketContext = React.createContext();
