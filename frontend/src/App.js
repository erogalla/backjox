import React, { useEffect } from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Home from './components/HomeRoute';

import TestHost from './components/TestHostRoute';
import TestHost2 from './components/TestHostRoute2';
import TestClient from './components/TestClientRoute';
import Canvas from './components/Canvas';

import {socket, SocketContext} from './socket'
import OutputRoute from './components/OutputRoute';
import styled from 'styled-components';
import TruthsInputRoute from "./components/TruthsInputRoute";
import UnityGame from "./components/UnityGame";


const GlobalWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const MainWrapper = styled.div`
  flex: 1;
`;


function App() {
  useEffect(() => {
    socket.connect()
    document.body.style.overflow = "hidden";
    document.body.classList.add("no-scroll")
  })

  return (
    <Router>
      <GlobalWrapper>
        <MainWrapper>
          <Switch>
            <SocketContext.Provider value={socket}>
              <Route path='/' exact component={Home} />
              <Route path='/input' exact component={TruthsInputRoute} />
              <Route path='/output' exact component={OutputRoute} />
              <Route path='/game' exact component={UnityGame} />
              <Route path='/testhost' exact component={TestHost} />
              <Route path='/testhost2' exact component={TestHost2} />
              <Route path='/testclient' exact component={TestClient} />
              <Route path='/testcanvas' exact component={Canvas} />
            </SocketContext.Provider>
          </Switch>
        </MainWrapper>
      </GlobalWrapper>
    </Router>
  );
}

export default App;
