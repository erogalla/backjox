// using System.Collections.Generic;
// using System;
// using socket.io;
// using UnityEngine;
// using UnityEngine.SceneManagement;
// using System.Collections;

// public class TwoTruthsOneLieButTheBetterOne : MonoBehaviour
// {
//     public static TwoTruthsOneLieButTheBetterOne _instance;

//     // Manager state
//     private Socket _socket;
    
//     // Game state
//     private string _roomCode;
//     public static string RoomCode => _instance._roomCode;

//     // Create a dictionary of players
//     private Dictionary<string, Player> _players = new Dictionary<string, Player>();

//     int responesLeft = 0;

//     public event Action<Player> onPlayerJoined;
//     public event Action<int> onResponseRoundStarted;

//     // public event Action<Player> ;
    
//     private void Awake()
//     {
//         if (_instance)
//         {
//             Destroy(gameObject);
//         }
//         else
//         {
//             _instance = this;
//             DontDestroyOnLoad(this);
//         }
//     }
    
//     private void Start()
//     {
//         // TODO: Add server location if once built
//         _socket = Socket.Connect("http://localhost:5000");
//         _socket.On(SystemEvents.connectError, HandleConnectError);
//         _socket.On(SystemEvents.connectTimeOut, HandleConnectTimeout);
//         _socket.On(SystemEvents.connect, HandleConnected);
//         _socket.On(SystemEvents.disconnect, HandleDisconnected);
//         _socket.On("codeCreated", HandleCodeCreated);
//         _socket.On("userJoined", HandleUserJoined);
//         _socket.On("userDisconnected", HandleUserLeft);

//         _socket.On("leaderStartedGame", HandleLeaderStartedGame);

//         _socket.On("recievedResponse", HandleReceivedResponse);
//         _socket.On("responseRoundOver", HandleResponseRoundOver);
//         // _socket.On("votingRoundOver", HandleVotingRoundOver);
//         // _socket.On("leaderboard", HandleLeaderboardStats);
//     }

//     private static void HandleConnectError(Exception e)
//     {
//         Debug.Log("Connect errored: " + e);
//     }

//     private static void HandleConnectTimeout()
//     {
//         Debug.Log("Connect timed out");
//     }

//     private void HandleConnected()
//     {
//         Debug.Log("Connected");
//     }

//     private static void HandleDisconnected()
//     {
//         Debug.Log("Disconnected");
//     }

//     #region Game Hosting

//     public static void TryHostGame()
//     {
//         _instance._socket.Emit("hostGame", "lie");
//     }

//     private void HandleCodeCreated(string code)
//     {
//         Debug.Log("Code created: " + code);
//         // I think 'code' comes in as JSON; scrub the quotes
//         _roomCode = code.Replace("\"", "");
//         SceneManager.LoadScene("LoadPlayersMenu");
//     }

//     // Will add the player to the leaderboard.
//     private void HandleUserJoined(string data)
//     {
//         Debug.Log("User joined: " + data);
//         string[] splitData = data.Replace("\"", "").Split(',');
//         string playerName = splitData[0];
//         string playerID = splitData[1];
//         Player player = new Player(playerName, playerID);
//         _players[playerID] = player;
//         if (onPlayerJoined != null) {
//             onPlayerJoined(player);
//         }
//     }

//     private void HandleUserLeft(string data)
//     {
//         Debug.Log("User left: " + data);
//         string playerID = data.Replace("\"", "");

//         // remove player from dictionary
//         _players.Remove(playerID);
//     }

//     // When the leader clicks "Start Game"
//     private void HandleLeaderStartedGame(string data) {
//         // TODO: Play animation about game rules (with voiceover)
        
//         // Actually start the response round
//         _instance._socket.Emit("startResponseRound", 60);

//         // Display timer ...
//     }

//     private void HandleReceivedResponse(string data)
//     {
//         Debug.Log("Received response: " + data);
//         // get all strings matching regex
//         string[] splitData = data.Replace("\"", "").Split(',');
//         string playerID = splitData[0];
//         string truth1 = splitData[1];
//         string truth2 = splitData[2];
//         string lie = splitData[3];

//         _players[playerID].truth1 = truth1;
//         _players[playerID].truth2 = truth2;
//         _players[playerID].lie = lie;

//         respones++;
//     }

//     private void HandleResponseRoundOver(string data)
//     {
//         // Occur if times up (from the server) or all responses are in

//         // Play timer ring if we haven't already played it.

//         Debug.Log("Response round over: " + data);
//         // TODO: Display instructions for the voting round of the game.

//         // Actually start voting round
//         startVotingRound();
//     }

//     // Play all the voting rounds, that is 1 round for every response.
//     private void startVotingRound() {
//         if (responesLeft > 0) {
//             _instance._socket.Emit("startVotingRound", 10);
//         } else  {
//             _instance._socket.Emit("endGame");
//             endGame();
//         }
        


//     }

//     private void endGame() {
//         // TODO: Play animation that ends the game
//     }



//     #endregion
// }
