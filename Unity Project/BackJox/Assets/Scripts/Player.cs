using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Player
{
  public string name;
  public string id;
  public Color color;

  public string truth1, truth2, lie;

  // constructor
  public Player(string name, string id)
  {
    this.name = name;
    this.id = id;
  }

  public Player(string name, string id, string color)
  {
    this.name = name;
    this.id = id;
    switch (color.ToLower())
    {
      case "red":
        this.color = Color.red;
        break;
      case "blue":
        this.color = Color.blue;
        break;
      case "green":
        this.color = Color.green;
        break;
      case "orange":
        this.color = new Color(1, 0.647f, 0, 1);
        break;
      case "purple":
        this.color = new Color(0.5f, 0, 0.5f, 1);
        break;
      case "black":
        this.color = Color.black;
        break;
      default:
        this.color = Color.gray;
        break;
    }
  }
}