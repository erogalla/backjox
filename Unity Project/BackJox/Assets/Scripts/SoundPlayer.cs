using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    private static SoundPlayer _instance;

    private AudioSource _source;
    
    // Should be static, but I want to pick these in the editor
    [SerializeField] private AudioClip welcome;
    [SerializeField] private AudioClip gameIntro;
    [SerializeField] private AudioClip hurry;
    [SerializeField] private AudioClip timeUp;
    [SerializeField] private AudioClip vote;
    [SerializeField] private AudioClip leader;
    [SerializeField] private AudioClip bloop;
    [SerializeField] private AudioClip whooshUp;
    [SerializeField] private AudioClip whooshDown;
    [SerializeField] private AudioClip reveal;
    [SerializeField] private AudioClip markerSound;

    private void Awake()
    {
        _instance = this;
        _source = gameObject.AddComponent<AudioSource>();
    }

    private void PlaySound(AudioClip clip)
    {
        _source.PlayOneShot(clip);
    }

    public static void PlayWelcome()
    {        
        _instance.PlaySound(_instance.welcome);
    }

    public static void PlayGameIntro()
    {
        _instance.PlaySound(_instance.gameIntro);
    }

    public static void PlayHurry()
    {
        _instance.PlaySound(_instance.hurry);
    }

    public static void PlayVote()
    {
        _instance.PlaySound(_instance.vote);
    }

    public static void PlayTimeUp()
    {
        _instance.PlaySound(_instance.timeUp);
    }

    public static void PlayLeader()
    {
        _instance.PlaySound(_instance.leader);
    }

    public static void PlayBloop()
    {
        _instance.PlaySound(_instance.bloop);
    }

    public static void PlayWhoosh(bool up)
    {
        _instance.PlaySound(up ? _instance.whooshUp : _instance.whooshDown);
    }

    public static void PlayReveal()
    {
        _instance.PlaySound(_instance.reveal);
    }

    public static void PlayMarkerSound()
    {
        if (_instance._source.isPlaying) return;
        _instance.PlaySound(_instance.markerSound);
    }
}
