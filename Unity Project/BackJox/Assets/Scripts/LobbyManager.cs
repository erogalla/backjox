using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class LobbyManager : MonoBehaviour
{

    [SerializeField] private TMP_Text RoomCodeText;
    [SerializeField] private GameObject NameListPanel;
    [SerializeField] private GameObject NamePrefab;
    [SerializeField] private Animator LobbyAnimator;
    [SerializeField] private TwoTruthsOneLie twoTruthsOneLie;

    // Start is called before the first frame update
    void Start()
    {
        RoomCodeText.SetText("----");
        LobbyAnimator = GetComponent<Animator>();

        // kill all children of namelistpanel
        foreach (Transform child in NameListPanel.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    // method to add nameprefab to namelistpanel
    public void AddName(Player player)
    {
        GameObject newName = Instantiate(NamePrefab, NameListPanel.transform);
        newName.name = player.id;
        newName.GetComponentInChildren<TMP_Text>().SetText(player.name);
        SoundPlayer.PlayBloop();
    }

    // method to remove nameprefab from namelistpanel
    public void RemoveName(Player player)
    {
        foreach (Transform child in NameListPanel.transform)
        {
            if (child.name == player.id)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    // set roomcode text
    public void SetRoomCode(string code)
    {
        RoomCodeText.SetText(code);
    }

    // show lobby
    public void ShowLobby()
    {
        LobbyAnimator.SetTrigger("ShowLobby");
        SoundPlayer.PlayWhoosh(true);
    }

    // hide lobby
    public void HideLobby()
    {
        LobbyAnimator.SetTrigger("HideLobby");
        SoundPlayer.PlayWhoosh(false);
    }

    public void ShowPlayerInputPanel()
    {
        twoTruthsOneLie.ShowPlayerInputPanel();
    }
}
