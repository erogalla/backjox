using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LeaderboardManager : MonoBehaviour
{
    [SerializeField] private TMP_Text firstPlaceText; 
    [SerializeField] private TMP_Text firstPlacePercentText; 
    [SerializeField] private TMP_Text secondPlaceText; 
    [SerializeField] private TMP_Text secondPlacePercentText; 
    [SerializeField] private TMP_Text thirdPlaceText; 
    [SerializeField] private TMP_Text thirdPlacePercentText; 
    [SerializeField] private Animator animator; 

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        firstPlaceText.text = "";
        secondPlaceText.text = "";
        thirdPlaceText.text = "";
        firstPlacePercentText.text = "";
        secondPlacePercentText.text = "";
        thirdPlacePercentText.text = "";
    }

    public void SetInfo(string firstPlace, string firstPlacePercent, string secondPlace, string secondPlacePercent, string thirdPlace, string thirdPlacePercent)
    {
        firstPlaceText.text = firstPlace;
        secondPlaceText.text = secondPlace;
        thirdPlaceText.text = thirdPlace;
        firstPlacePercentText.text = firstPlacePercent;
        secondPlacePercentText.text = secondPlacePercent;
        thirdPlacePercentText.text = thirdPlacePercent;
    }

    // show lobby
  public void ShowLeaderboard()
  {
    animator.SetTrigger("ShowLeaderboard");
    SoundPlayer.PlayWhoosh(true);
  }

  // hide lobby
  public void HideLeaderboard()
  {
    animator.SetTrigger("HideLeaderboard");
    SoundPlayer.PlayWhoosh(false);
  }
}
