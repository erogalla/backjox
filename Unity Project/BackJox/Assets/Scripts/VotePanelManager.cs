using System.Collections.Generic;
using System;
using socket.io;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using TMPro;
using UnityEngine.UI;

public class VotePanelManager : MonoBehaviour
{
  [SerializeField] private TwoTruthsOneLie twoTruthsOneLie;
  [SerializeField] private Animator animator;
  [SerializeField] private TMP_Text nameText;
  [SerializeField] private TMP_Text optionAText;
  [SerializeField] private TMP_Text optionBText;
  [SerializeField] private TMP_Text optionCText;
  [SerializeField] private Timer voteTimer;
  [SerializeField] private GameObject NameListPanel;
  [SerializeField] private GameObject NamePrefab;
  [SerializeField] private Slider optionASlider;
  [SerializeField] private Slider optionBSlider;
  [SerializeField] private Slider optionCSlider;
  [SerializeField] private TMP_Text optionALabel;
  [SerializeField] private TMP_Text optionBLabel;
  [SerializeField] private TMP_Text optionCLabel;
  [SerializeField] private Image optionAImage;
  [SerializeField] private Image optionBImage;
  [SerializeField] private Image optionCImage;


  private float optionAVotePercent, optionBVotePercent, optionCVotePercent;
  private string correctAnswer;

  // Start is called before the first frame update
  void Start()
  {
    animator = GetComponent<Animator>();
    nameText.text = "";
    optionAText.text = "";
    optionBText.text = "";
    optionCText.text = "";
    optionAVotePercent = optionBVotePercent = optionCVotePercent = -1;
    correctAnswer = "";
    ResetStats();
  }

  void Update()
  {
    if ((optionAVotePercent != -1 || optionBVotePercent != -1 || optionCVotePercent != -1)
        && !(sliderFinished(optionASlider.value, optionAVotePercent) && sliderFinished(optionBSlider.value, optionBVotePercent)
        && sliderFinished(optionCSlider.value, optionCVotePercent)))
    {
      if (voteTimer.isStarted())
      {
        voteTimer.StopTimer();
      }

      // lerp the sliders
      optionASlider.value = Mathf.Lerp(optionASlider.value, optionAVotePercent, Time.deltaTime * UnityEngine.Random.Range(2, 7));
      optionBSlider.value = Mathf.Lerp(optionBSlider.value, optionBVotePercent, Time.deltaTime * UnityEngine.Random.Range(2, 7));
      optionCSlider.value = Mathf.Lerp(optionCSlider.value, optionCVotePercent, Time.deltaTime * UnityEngine.Random.Range(2, 7));
    }
    else if (correctAnswer != "")
    {
      optionALabel.text = (optionAVotePercent * 100).ToString("0") + "%";
      optionBLabel.text = (optionBVotePercent * 100).ToString("0") + "%";
      optionCLabel.text = (optionCVotePercent * 100).ToString("0") + "%";
      if (optionAText.text == correctAnswer)
      {
        optionAImage.color = new Color(0, 1, 0, 0.2f);
      }
      else if (optionBText.text == correctAnswer)
      {
        optionBImage.color = new Color(0, 1, 0, 0.2f);
      }
      else if (optionCText.text == correctAnswer)
      {
        optionCImage.color = new Color(0, 1, 0, 0.2f);
      }
      SoundPlayer.PlayReveal();
      StartCoroutine(DoneShowingVotingStats());
      correctAnswer = "";
    }
  }

  public bool sliderFinished(float value, float target)
  {
    return Mathf.Abs(value - target) < 0.01f;
  }


  // coroutine to wait 5 seconds before DoneShowingVotingStats()
  IEnumerator DoneShowingVotingStats()
  {
    animator.ResetTrigger("HideVotePanel");
    yield return new WaitForSeconds(5);
    HideVotePanel();
    yield return new WaitForSeconds(1.5f);
    twoTruthsOneLie.TryStartVotingRound();
  }

  public void SetInfo(string name, string playerID, string optionA, string optionB, string optionC)
  {
    nameText.text = name;
    nameText.name = playerID;
    optionAText.text = optionA;
    optionBText.text = optionB;
    optionCText.text = optionC;
  }

  public void ResetNamePanel(Dictionary<string, Player> players)
  {
    foreach (Transform child in NameListPanel.transform)
    {
      Destroy(child.gameObject);
    }
    foreach (Player player in players.Values)
    {
      Debug.Log(player.name);
      if (player.id == nameText.name)
      {
        continue;
      }
      GameObject newName = Instantiate(NamePrefab, NameListPanel.transform);
      newName.name = player.id;
      newName.GetComponentInChildren<TMP_Text>().text = player.name;
    }
  }

  public void RemoveName(Player player)
  {
    foreach (Transform child in NameListPanel.transform)
    {
      if (child.name == player.id)
      {
        GameObject.Destroy(child.gameObject);
      }
    }
  }

  public void UpdateStats(string truth1Text, float truth1Percent, string truth2Text, float truth2Percent, string lieText, float liePercent)
  {
    // Set Option with truth1 percentage
    if (truth1Text == optionAText.text)
    {
      optionAVotePercent = truth1Percent;
    }
    else if (truth1Text == optionBText.text)
    {
      optionBVotePercent = truth1Percent;
    }
    else if (truth1Text == optionCText.text)
    {
      optionCVotePercent = truth1Percent;
    }

    // Set Option with truth2 percentage
    if (truth2Text == optionAText.text)
    {
      optionAVotePercent = truth2Percent;
    }
    else if (truth2Text == optionBText.text)
    {
      optionBVotePercent = truth2Percent;
    }
    else if (truth2Text == optionCText.text)
    {
      optionCVotePercent = truth2Percent;
    }

    // Set Option with lie percentage
    if (lieText == optionAText.text)
    {
      optionAVotePercent = liePercent;
    }
    else if (lieText == optionBText.text)
    {
      optionBVotePercent = liePercent;
    }
    else if (lieText == optionCText.text)
    {
      optionCVotePercent = liePercent;
    }

    this.correctAnswer = lieText;
  }

  public void ResetStats()
  {
    optionAVotePercent = optionBVotePercent = optionCVotePercent = -1;
    correctAnswer = "";
    optionALabel.text = "A";
    optionBLabel.text = "B";
    optionCLabel.text = "C";
    optionASlider.value = 0;
    optionBSlider.value = 0;
    optionCSlider.value = 0;
    optionAImage.color = new Color(0, 0, 0, 0.0f);
    optionBImage.color = new Color(0, 0, 0, 0.0f);
    optionCImage.color = new Color(0, 0, 0, 0.0f);
  }

  public void StartVoteTimer(float time)
  {
    voteTimer.StartTimer(time);
  }

  // show lobby
  public void ShowVotePanel()
  {
    animator.SetTrigger("ShowVotePanel");
    SoundPlayer.PlayWhoosh(true);
  }

  // hide lobby
  public void HideVotePanel()
  {
    animator.SetTrigger("HideVotePanel");
    SoundPlayer.PlayWhoosh(false);
  }
}
