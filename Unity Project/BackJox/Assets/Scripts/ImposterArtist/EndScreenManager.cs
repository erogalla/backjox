using System;
using UnityEngine;


public class EndScreenManager : MonoBehaviour
{

  [SerializeField] private Animator animator;
  [SerializeField] private GameObject imposterWonPanel;
  [SerializeField] private GameObject imposterLostPanel;
  public bool imposterWon;

  // Start is called before the first frame update
  void Start()
  {
    animator = GetComponent<Animator>();
    imposterWonPanel.SetActive(false);
    imposterLostPanel.SetActive(false);
  }

  // show lobby
  public void Show()
  {
    animator.SetTrigger("ShowEndScreenPanel");
    SoundPlayer.PlayWhoosh(true);

    imposterWonPanel.SetActive(imposterWon);
    imposterLostPanel.SetActive(!imposterWon);
  }

  public void SetStatus(bool imposterWon)
  {
    this.imposterWon = imposterWon;
  }

  // hide lobby
  public void Hide()
  {
    animator.SetTrigger("HideEndScreenPanel");
    SoundPlayer.PlayWhoosh(false);
  }

}
