using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class DrawPanelManager : MonoBehaviour
{

  [SerializeField] private GameObject nameList;
  [SerializeField] private GameObject NamePrefab;
  [SerializeField] private Animator animator;
  [SerializeField] private TMP_Text titleText;
  public Timer responseTimer;
  private Whiteboard whiteboard;
  [SerializeField] private ImposterArtist imposterArtist;
  [SerializeField] private Slider inkProgress;



  // Start is called before the first frame update
  void Start()
  {

    whiteboard = GetComponentInChildren<Whiteboard>();
    animator = GetComponent<Animator>();
    inkProgress.value = 1;
    ClearNames();
  }

  public void ClearNames()
  {
    foreach (Transform child in nameList.transform)
    {
      Destroy(child.gameObject);
    }
  }

  public void AddAllNames(Dictionary<string, Player> players)
  {
    foreach (Player player in players.Values)
    {
      AddName(player);
    }
  }

  public void resetAllBGColor()
  {
    foreach (Transform child in nameList.transform)
    {
      HorizontalNameTag nameTag = child.GetComponent<HorizontalNameTag>();
      nameTag.SetBGColor(nameTag.textColor);
    }
  }

  public void HighlightName(Player player)
  {
    foreach (Transform child in nameList.transform)
    {
      HorizontalNameTag nameTag = child.GetComponent<HorizontalNameTag>();
      if (child.name == player.id)
      {
        nameTag.SetBGColor(player.color, 0.3f);
        nameTag.SetTextColor(player.color);
        Debug.Log("Highlighted " + player.name);
      }
      else
      {
        // nameTag.SetBGColor(player.color, 0.3f);
        // nameTag.SetTextColor(player.color);
        nameTag.SetBGColor(Color.white);
        nameTag.SetTextColor(Color.white);
      }
    }
  }

  public void Draw(List<PixelInfo> pixels)
  {
    whiteboard.SetPixels(pixels);
  }

  public void SetInkProgress(float progress)
  {
    inkProgress.value = progress;
  }

  public void AddName(Player player)
  {
    GameObject newName = Instantiate(NamePrefab, nameList.transform);
    HorizontalNameTag nameTag = newName.GetComponent<HorizontalNameTag>();
    nameTag.name = player.id;
    nameTag.SetText(player.name);
    nameTag.SetTextColor(player.color);
    nameTag.SetBGColor(player.color);
  }

  // show lobby
  public void Show(Player player)
  {
    animator.SetTrigger("ShowDrawPanel");
    titleText.text = player.name.ToUpper() + " IS DRAWING!";
    SoundPlayer.PlayWhoosh(true);
  }

  // hide lobby
  public void Hide()
  {
    animator.SetTrigger("HideDrawPanel");
    SoundPlayer.PlayWhoosh(false);
  }

  public void StartTimer(float time)
  {
    responseTimer.StartTimer(time);
  }

  public void StopTimer()
  {
    responseTimer.StopTimer();
  }

  // Update is called once per frame
  void Update()
  {

  }
}
