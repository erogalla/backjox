using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ImposterVotePanelManager : MonoBehaviour
{
  [SerializeField] private Animator animator;
  public Timer timer;
  [SerializeField] private GameObject NameListPanel;
  [SerializeField] private GameObject NamePrefab;
  [SerializeField] private ImposterArtist imposterArtist;


  void Start()
  {
    animator = GetComponent<Animator>();
  }
  public void Show()
  {
    animator.SetTrigger("ShowImposterVotePanel");
    SoundPlayer.PlayWhoosh(true);
  }

  public void Hide()
  {
    animator.SetTrigger("HideImposterVotePanel");
    SoundPlayer.PlayWhoosh(false);
  }

  public void StartTimer(float time)
  {
    timer.StartTimer(time);
  }

  public void StopTimer()
  {
    timer.StopTimer();
  }

  public void ResetNamePanel(Dictionary<string, Player> players)
  {
    foreach (Transform child in NameListPanel.transform)
    {
      Destroy(child.gameObject);
    }
    foreach (Player player in players.Values)
    {
      GameObject newName = Instantiate(NamePrefab, NameListPanel.transform);
      newName.name = player.id;
      newName.GetComponentInChildren<TMP_Text>().text = player.name;
    }
  }

  public void RemoveName(Player player)
  {
    foreach (Transform child in NameListPanel.transform)
    {
      if (child.name == player.id)
      {
        GameObject.Destroy(child.gameObject);
      }
    }
  }

  public void ShowEndScreen() 
  {
    imposterArtist.ShowEndScreen();
  }

}
