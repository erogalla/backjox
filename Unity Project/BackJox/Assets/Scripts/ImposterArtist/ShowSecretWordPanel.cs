using System;
using UnityEngine;


public class ShowSecretWordPanel : MonoBehaviour
{

  [SerializeField] private Animator animator;
  public Timer responseTimer;
  [SerializeField] private ImposterArtist imposterArtist;


  // Start is called before the first frame update
  void Start()
  {
    animator = GetComponent<Animator>();
  }

  // show lobby
  public void Show()
  {
    animator.SetTrigger("ShowSecretWordPanel");
    SoundPlayer.PlayWhoosh(true);
  }

  // hide lobby
  public void Hide()
  {
    animator.SetTrigger("HideSecretWordPanel");
    SoundPlayer.PlayWhoosh(false);
  }

  public void StartResponseTimer(float time)
  {
    responseTimer.StartTimer(time);
  }

  public void StopResponseTimer()
  {
    responseTimer.StopTimer();
  }
}
