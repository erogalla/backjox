using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class HorizontalNameTag : MonoBehaviour
{
    private TMP_Text text;
    public Color textColor;
    [SerializeField] private Color bgColor;

    private void Awake()
    {
        text = GetComponentInChildren<TMP_Text>();
    }

    public void SetText(string text)
    {
        this.text.text = text;
    }

    public void SetTextColor(Color color)
    {
        this.textColor = color;
        text.color = color;
    }

    public void SetBGColor(Color color, float alpha = 0.1f)
    {
        this.bgColor = color;
        GetComponentInChildren<Image>().color = new Color(color.r, color.g, color.b, alpha);
    }
}
