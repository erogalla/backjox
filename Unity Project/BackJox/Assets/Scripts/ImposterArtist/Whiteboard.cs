using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Whiteboard : MonoBehaviour
{

  public Texture2D texture;
  private Sprite mySprite;
  public Vector2 textureSize = new Vector2(800, 1000);
  private Image image;

  private void Awake()
  {
    image = GetComponent<Image>();
  }

  void Start()
  {
    texture = new Texture2D((int)textureSize.x, (int)textureSize.y);
    for (int x = 0; x < texture.width; x++)
    {
      for (int y = 0; y < texture.height; y++)
      {
        texture.SetPixel(x, y, Color.white);
      }
    }
    texture.Apply();

    mySprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    image.sprite = mySprite;
    image.preserveAspect = true;
  }

  public void SetPixels(List<PixelInfo> pixels)
  {
    SoundPlayer.PlayMarkerSound();
    foreach (PixelInfo pixel in pixels)
    {
      for (int x = (int)pixel.x - 10; x < pixel.x + 10; x++)
      {
        for (int y = (int)pixel.y - 10; y < pixel.y + 10; y++)
        {
          if (x >= 0 && x < texture.width && y >= 0 && y < texture.height)
          {
            texture.SetPixel(x, y, pixel.color);
          }
        }
      }
      texture.Apply();
    }
  }
    // private void Update()
    // {
      // if (Input.GetMouseButton(0)) {
      //   Debug.Log("Mouse down");

      //   Vector2 mousePosition = Input.mousePosition;
      //   mousePosition.x = mousePosition.x / Screen.width;
      //   mousePosition.y = mousePosition.y / Screen.height;
      //   mousePosition.x *= textureSize.x;
      //   mousePosition.y *= textureSize.y;

      //   // get circle of pixels around mouse
      //   List<PixelInfo> pixels = new List<PixelInfo>();
      //   for (int x = (int)mousePosition.x - 10; x < mousePosition.x + 10; x++)
      //   {
      //     for (int y = (int)mousePosition.y - 10; y < mousePosition.y + 10; y++)
      //     {
      //       if (x >= 0 && x < texture.width && y >= 0 && y < texture.height)
      //       {
      //         pixels.Add(new PixelInfo(x, y, Color.black));
      //       }
      //     }
      //   }

      //   SetPixels(pixels);


      //   // Debug.Log(mousePosition);
      //   // texture.SetPixel((int)mousePosition.x, (int)mousePosition.y, Color.green);
      //   // texture.Apply();
      // }
    // }
  }


  public class PixelInfo
  {
    public int x;
    public int y;
    public Color color;
    public PixelInfo(int x, int y, Color color)
    {
      this.x = x;
      this.y = y;
      this.color = color;
    }
  }
