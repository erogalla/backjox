using System.Collections.Generic;
using System;
using socket.io;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
public class ImposterArtist : MonoBehaviour
{
  // Manager state
  private Socket _socket;
  private Dictionary<string, Player> _players = new Dictionary<string, Player>();
  private string _roomCode;


  // Scene Related
  private LobbyManager lobbyManager;
  [SerializeField] private GameObject LobbyPanel;
  [SerializeField] private ShowSecretWordPanel showSecretWordPanel;
  [SerializeField] private DrawPanelManager drawPanelManager;
  [SerializeField] private ImposterVotePanelManager imposterVotePanelManager;
  [SerializeField] private EndScreenManager endScreenManager;


  

  private void Awake()
  {
    // "http://localhost:5000"
    // "http://backjox.herokuapp.com/"

    // _socket = Socket.Connect("http://backjox.herokuapp.com/");
    _socket = Socket.Connect("http://localhost:5000");
    _socket.On(SystemEvents.connectError, HandleConnectError);
    _socket.On(SystemEvents.connectTimeOut, HandleConnectTimeout);
    _socket.On(SystemEvents.connect, HandleConnected);
    _socket.On(SystemEvents.disconnect, HandleDisconnected);
    _socket.On("codeCreated", HandleCodeCreated);
    _socket.On("userJoined", HandleUserJoined);
    _socket.On("userDisconnected", HandleUserLeft);
    _socket.On("leaderStartedGame", HandleLeaderStartedGame);
    _socket.On("secretWord", HandleSecretWord);
    _socket.On("hideSecret", HandleHideSecret);
    _socket.On("startDrawingRound", HandleStartDrawingRound);
    _socket.On("draw", HandleDraw);
    _socket.On("drawingRoundOver", HandleDrawingRoundsOver);
    _socket.On("allDrawingRoundsOver", HandleAllDrawingRoundsOver);
    _socket.On("votingRoundStarted", HandleVotingStarted);
    _socket.On("recievedVote", HandleReceivedVote);
    _socket.On("votedImposter", HandleVotedImposter);
    _socket.On("votedWrong", HandleVotedWrong);


  }

  // Start is called before the first frame update
  void Start()
  {
    lobbyManager = LobbyPanel.GetComponent<LobbyManager>();
    LobbyPanel.SetActive(true);
  }

  private static void HandleConnectError(Exception e)
  {
    Debug.Log("Connect errored: " + e);
  }

  private static void HandleConnectTimeout()
  {
    Debug.Log("Connect timed out");
  }

  private void HandleConnected()
  {
    Debug.Log("Connected");
    // Host the Game and get the code
    _socket.Emit("hostGame", "impostorArtist");
  }

  private static void HandleDisconnected()
  {
    Debug.Log("Disconnected");
  }

  private void HandleCodeCreated(string code)
  {
    Debug.Log("Code created: " + code);
    // I think 'code' comes in as JSON; scrub the quotes
    _roomCode = code.Replace("\"", "");
    lobbyManager.SetRoomCode(_roomCode.ToUpper());

    // SoundPlayer.PlayWelcome();
  }

  // Will add the player to the leaderboard.
  private void HandleUserJoined(string data)
  {
    Debug.Log("User joined: " + data);
    string[] splitData = data.Replace("\"", "").Split(',');
    string playerName = splitData[0];
    string playerID = splitData[1];
    string playerColor = splitData[2];
    Player player = new Player(playerName, playerID, playerColor);
    _players[playerID] = player;
    lobbyManager.AddName(player);
  }

  private void HandleUserLeft(string data)
  {
    Debug.Log("User left: " + data);
    string playerID = data.Replace("\"", "");

    // remove player from dictionary
    Player player = _players[playerID];
    lobbyManager.RemoveName(player);
    _players.Remove(playerID);
  }

  // When the leader clicks "Start Game"
  private void HandleLeaderStartedGame(string data)
  {
    Debug.Log("Leader started game");
    // TODO: Play animation about game rules (with voiceover)
    lobbyManager.HideLobby();

    // Actually start the response round
    _socket.Emit("displaySecret", "10");

    // SoundPlayer.PlayGameIntro();
  }

  public void HandleSecretWord(string data) 
  {
    int roundTime = int.Parse(data.Replace("\"", "").Split(',')[1]);
    Debug.Log("Secret word shown: " + roundTime);
    showSecretWordPanel.Show();
    showSecretWordPanel.StartResponseTimer(roundTime);
  }

  public void HandleHideSecret(string data) 
  {
    Debug.Log("Secret word done showing");
    showSecretWordPanel.Hide();
    _socket.Emit("startDrawingRound", "30");
    drawPanelManager.AddAllNames(_players);
  }

  public void HandleStartDrawingRound(string data) 
  {
    string[] splitData = data.Replace("\"", "").Split(',');
    int roundTime = int.Parse(splitData[0]);
    string playerId = splitData[1];
    Player player = _players[playerId];
    drawPanelManager.Show(player);
    drawPanelManager.HighlightName(player);
    drawPanelManager.StartTimer(roundTime);
    drawPanelManager.SetInkProgress(1);
  }

  public void HandleDraw(string data) 
  {
    string[] splitData = data.Replace("\"", "").Split(',');
    Debug.Log("Draw: {" + splitData[0] + "} {" + splitData[1] + "} {" + splitData[2] + "} {" + splitData[3] + "}");
    int inkLeft = int.Parse(splitData[0]);
    string playerId = splitData[1];
    int x = int.Parse(splitData[2]);
    int y = int.Parse(splitData[3]);

    Player player = _players[playerId];
    Color color = player.color;
    List<PixelInfo> pixels = new List<PixelInfo>();
    pixels.Add(new PixelInfo(x, y, color));
    drawPanelManager.Draw(pixels);
    drawPanelManager.SetInkProgress((float)inkLeft / 200);
  }

  public void HandleDrawingRoundsOver(string data) 
  {
    drawPanelManager.Hide();
    drawPanelManager.StopTimer();
    _socket.Emit("startDrawingRound", "30");
  }

  public void HandleAllDrawingRoundsOver(string data) 
  {
    drawPanelManager.Hide();
    drawPanelManager.StopTimer();
    _socket.Emit("startVotingRound", "60");
  }

  public void HandleVotingStarted(string data) 
  {
    int roundTime = int.Parse(data.Replace("\"", ""));
    imposterVotePanelManager.Show();
    imposterVotePanelManager.ResetNamePanel(_players);
    imposterVotePanelManager.StartTimer(roundTime);
  }

  public void HandleReceivedVote(string data) 
  {
    Debug.Log("Received vote: " + data);
    string playerID = data.Replace("\"", "");
    Player player = _players[playerID];
    imposterVotePanelManager.RemoveName(player);
  }

  public void HandleVotedImposter(string data) 
  {
    Debug.Log("Voted imposter: " + data);
    endScreenManager.SetStatus(false);
    imposterVotePanelManager.Hide();
  }

  public void HandleVotedWrong(string data) 
  {
    Debug.Log("Voted wrong: " + data);
    endScreenManager.SetStatus(true);
    imposterVotePanelManager.Hide();
  }

  public void ShowEndScreen() 
  {
    endScreenManager.Show();
  }

  // quit game
  public void QuitGame()
  {
    Application.Quit();
  }

  // function to change to scene with name
  public void ChangeScene(string sceneName)
  {
    SceneManager.LoadScene(sceneName);
  }

  // Update is called once per frame
  void Update()
  {

  }
}