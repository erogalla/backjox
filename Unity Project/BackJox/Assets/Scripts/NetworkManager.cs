using System.Collections.Generic;
using System;
using socket.io;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class NetworkManager : MonoBehaviour
{
    public static NetworkManager _instance;

    // Manager state
    private Socket _socket;
    
    // Game state
    private string _roomCode;
    public static string RoomCode => _instance._roomCode;

    // Create a dictionary of players
    private Dictionary<string, Player> _players = new Dictionary<string, Player>();
    public event Action<Player> onPlayerJoined;
    public event Action<int> onResponseRoundStarted;

    // public event Action<Player> ;
    
    private void Awake()
    {
        if (_instance)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
    }
    
    private void Start()
    {
        _socket = Socket.Connect("http://localhost:5000");
        _socket.On(SystemEvents.connectError, HandleConnectError);
        _socket.On(SystemEvents.connectTimeOut, HandleConnectTimeout);
        _socket.On(SystemEvents.connect, HandleConnected);
        _socket.On(SystemEvents.disconnect, HandleDisconnected);
        _socket.On("codeCreated", HandleCodeCreated);
        _socket.On("userJoined", HandleUserJoined);
        _socket.On("userDisconnected", HandleUserLeft);

        _socket.On("responseRoundStarted", HandleResponseRoundStarted);
        _socket.On("recievedResponse", HandleReceivedResponse);
        _socket.On("responseRoundOver", HandleResponseRoundOver);
        _socket.On("votingRoundStarted", HandleVotingRoundStarted);
        // _socket.On("votingRoundOver", HandleVotingRoundOver);
        // _socket.On("leaderboard", HandleLeaderboardStats);
    }

    private static void HandleConnectError(Exception e)
    {
        Debug.Log("Connect errored: " + e);
    }

    private static void HandleConnectTimeout()
    {
        Debug.Log("Connect timed out");
    }

    private void HandleConnected()
    {
        Debug.Log("Connected");
    }

    private static void HandleDisconnected()
    {
        Debug.Log("Disconnected");
    }

    #region Game Hosting

    public static void TryHostGame()
    {
        _instance._socket.Emit("hostGame", "lie");
    }

    private void HandleCodeCreated(string code)
    {
        Debug.Log("Code created: " + code);
        // I think 'code' comes in as JSON; scrub the quotes
        _roomCode = code.Replace("\"", "");
        SceneManager.LoadScene("LoadPlayersMenu");
    }

    private void HandleUserJoined(string data)
    {
        Debug.Log("User joined: " + data);
        string[] splitData = data.Replace("\"", "").Split(',');
        string playerName = splitData[0];
        string playerID = splitData[1];
        Player player = new Player(playerName, playerID);
        _players[playerID] = player;
        if (onPlayerJoined != null) {
            onPlayerJoined(player);
        }
    }

    private void HandleUserLeft(string data)
    {
        Debug.Log("User left: " + data);
        string playerID = data.Replace("\"", "");

        // remove player from dictionary
        _players.Remove(playerID);
    }

    private void HandleResponseRoundStarted(string data)
    {
        Debug.Log("Response round started: " + data);
        int roundTime = int.Parse(data.Replace("\"", ""));
        if (onResponseRoundStarted != null) {
            onResponseRoundStarted(roundTime);
        }
    }

    private void HandleReceivedResponse(string data)
    {
        Debug.Log("Received response: " + data);
        // get all strings matching regex
        string[] splitData = data.Replace("\"", "").Split(',');
        string playerID = splitData[0];
        string truth1 = splitData[1];
        string truth2 = splitData[2];
        string lie = splitData[3];

        _players[playerID].truth1 = truth1;
        _players[playerID].truth2 = truth2;
        _players[playerID].lie = lie;
    }

    private void HandleResponseRoundOver(string data)
    {
        Debug.Log("Response round over: " + data);
        SceneManager.LoadScene("PlayerStatements");
    }

    private void HandleVotingRoundStarted(string data)
    {
        Debug.Log("Voting round started: " + data);
    }
    #endregion
}
