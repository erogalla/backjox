using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerInputManager : MonoBehaviour
{

  [SerializeField] private Animator animator;
  [SerializeField] private Timer responseTimer;
  [SerializeField] private GameObject NameListPanel;
  [SerializeField] private GameObject NamePrefab;
  [SerializeField] private TwoTruthsOneLie twoTruthsOneLie;

  // Start is called before the first frame update
  void Start()
  {
    animator = GetComponent<Animator>();

    // kill all children of namelistpanel
    foreach (Transform child in NameListPanel.transform)
    {
      GameObject.Destroy(child.gameObject);
    }
  }

  // method to add nameprefab to namelistpanel
  public void AddName(Player player)
  {
    GameObject newName = Instantiate(NamePrefab, NameListPanel.transform);
    newName.name = player.id;
    newName.GetComponentInChildren<TMP_Text>().SetText(player.name);
  }

  // show lobby
  public void ShowPlayerInputPanel()
  {
    animator.SetTrigger("ShowPlayerInputPanel");
    SoundPlayer.PlayWhoosh(true);
  }

  // hide lobby
  public void HidePlayerInputPanel()
  {
    animator.SetTrigger("HidePlayerInputPanel");
    SoundPlayer.PlayWhoosh(false);
  }

  public void StartResponseTimer(float time)
  {
    responseTimer.StartTimer(time);
  }

  public void StopResponseTimer()
  {
    responseTimer.StopTimer();
  }
}
