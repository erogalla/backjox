using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Timer : MonoBehaviour
{


  [SerializeField] private Image timerImage;
  [SerializeField] private TMP_Text timerText;

  private float duration = 0f;
  private float currentTime = 0f;
  private bool started = false;
  private bool hurried;

  public event Action onTimerEnded;

  // Start is called before the first frame update
  void Start()
  {
    timerImage.fillAmount = 1;
    timerText.text = "";
  }

  public void StartTimer(float duration)
  {
    this.duration = duration;
    currentTime = duration;
    started = true;
    hurried = false;
  }

  public void StopTimer()
  {
    started = false;
  }

  public bool isStarted()
  {
    return started;
  }

  // Update is called once per frame
  private void Update()
  {
    if (!started) return;

    currentTime -= Time.deltaTime;
    timerImage.fillAmount = currentTime / duration;
    timerText.text = currentTime.ToString("0.00");
    if (currentTime <= 0)
    {
      OnTimerEnded();
    }
    else if (!hurried && currentTime <= 10)
    {
      SoundPlayer.PlayHurry();
      hurried = true;
    }
  }

  public void OnTimerEnded()
  {
    onTimerEnded?.Invoke();
    SoundPlayer.PlayTimeUp();
    timerText.text = "0.00";
    timerImage.fillAmount = 0;
    started = false;
  }

}
