using System.Collections.Generic;
using System;
using socket.io;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
public class TwoTruthsOneLie : MonoBehaviour
{
  // Manager state
  private Socket _socket;
  private Dictionary<string, Player> _players = new Dictionary<string, Player>();
  private string _roomCode;
  private int recievedResponses;
  private int numPlayersShown;
  private bool votingStarted;


  // Actions

  // Scene Related
  private LobbyManager lobbyManager;
  [SerializeField] private GameObject LobbyPanel;
  [SerializeField] private PlayerInputManager playerInputManager;
  [SerializeField] private VotePanelManager votePanelManager;
  [SerializeField] private LeaderboardManager leaderboardManager;

  private void Awake()
  {
    _socket = Socket.Connect("http://backjox.herokuapp.com/");
    _socket.On(SystemEvents.connectError, HandleConnectError);
    _socket.On(SystemEvents.connectTimeOut, HandleConnectTimeout);
    _socket.On(SystemEvents.connect, HandleConnected);
    _socket.On(SystemEvents.disconnect, HandleDisconnected);
    _socket.On("codeCreated", HandleCodeCreated);
    _socket.On("userJoined", HandleUserJoined);
    _socket.On("userDisconnected", HandleUserLeft);
    _socket.On("leaderStartedGame", HandleLeaderStartedGame);
    _socket.On("responseRoundStarted", HandleResponseRoundStarted);
    _socket.On("recievedResponse", HandleReceivedResponse);
    _socket.On("responseRoundOver", HandleResponseRoundOver);
    _socket.On("votingRoundStarted", HandleVotingRoundStarted);
    _socket.On("recievedVote", HandleVoteReceived);
    _socket.On("votingRoundOver", HandleVotingRoundOver);
    _socket.On("leaderboard", HandleLeaderboardStats);

  }

  // Start is called before the first frame update
  void Start()
  {
    numPlayersShown = 0;
    lobbyManager = LobbyPanel.GetComponent<LobbyManager>();
    LobbyPanel.SetActive(true);
  }

  private static void HandleConnectError(Exception e)
  {
    Debug.Log("Connect errored: " + e);
  }

  private static void HandleConnectTimeout()
  {
    Debug.Log("Connect timed out");
  }

  private void HandleConnected()
  {
    Debug.Log("Connected");
    // Host the Game and get the code
    _socket.Emit("hostGame", "twoTruthsOneLie");
  }

  private static void HandleDisconnected()
  {
    Debug.Log("Disconnected");
  }

  private void HandleCodeCreated(string code)
  {
    Debug.Log("Code created: " + code);
    // I think 'code' comes in as JSON; scrub the quotes
    _roomCode = code.Replace("\"", "");
    lobbyManager.SetRoomCode(_roomCode.ToUpper());

    SoundPlayer.PlayWelcome();
  }

  // Will add the player to the leaderboard.
  private void HandleUserJoined(string data)
  {
    Debug.Log("User joined: " + data);
    string[] splitData = data.Replace("\"", "").Split(',');
    string playerName = splitData[0];
    string playerID = splitData[1];
    Player player = new Player(playerName, playerID);
    _players[playerID] = player;
    lobbyManager.AddName(player);
  }

  private void HandleUserLeft(string data)
  {
    Debug.Log("User left: " + data);
    string playerID = data.Replace("\"", "");

    // remove player from dictionary
    Player player = _players[playerID];
    lobbyManager.RemoveName(player);
    _players.Remove(playerID);
  }

  // When the leader clicks "Start Game"
  private void HandleLeaderStartedGame(string data)
  {
    Debug.Log("Leader started game");
    // TODO: Play animation about game rules (with voiceover)
    lobbyManager.HideLobby();

    // Actually start the response round
    _socket.Emit("startResponseRound", "90");

    SoundPlayer.PlayGameIntro();
  }

  private void HandleResponseRoundStarted(string data)
  {
    Debug.Log("Response round started: " + data);
    int roundTime = int.Parse(data.Replace("\"", ""));
    playerInputManager.StartResponseTimer(roundTime);
  }

  private void HandleReceivedResponse(string data)
  {
    Debug.Log("Received response: " + data);
    recievedResponses++;
    // get all strings matching regex
    string[] splitData = data.Replace("\"", "").Split(',');
    string playerID = splitData[0];
    string truth1 = splitData[1];
    string truth2 = splitData[2];
    string lie = splitData[3];

    _players[playerID].truth1 = truth1;
    _players[playerID].truth2 = truth2;
    _players[playerID].lie = lie;

    playerInputManager.AddName(_players[playerID]);
    SoundPlayer.PlayBloop();
  }

  private void HandleResponseRoundOver(string data)
  {
    Debug.Log("Response round over: " + data);
    playerInputManager.HidePlayerInputPanel();
    playerInputManager.StopResponseTimer();
    TryStartVotingRound();
  }


  public void TryStartVotingRound()
  {
    Debug.Log(numPlayersShown + " < " + recievedResponses);
    if (numPlayersShown < recievedResponses)
    {
      Debug.Log("LETS GO AGAIN");
      _socket.Emit("startVotingRound", "45");
    }
    else if (numPlayersShown == recievedResponses)
    {
      _socket.Emit("endGame");
    }

    if (!votingStarted)
    {
      SoundPlayer.PlayVote();
      votingStarted = true;
    }
  }

  public void HandleVotingRoundStarted(string data)
  {
    Debug.Log("Voting round started: " + data);
    numPlayersShown++;

    string[] splitData = data.Replace("\"", "").Split(',');
    string currentVotingRound = splitData[0];
    string name = splitData[1];
    float votingRoundTime = float.Parse(splitData[2]);
    string firstOption = splitData[3];
    string secondOption = splitData[4];
    string thirdOption = splitData[5];
    string playerID = splitData[6];

    votePanelManager.ShowVotePanel();
    votePanelManager.SetInfo(name, playerID, firstOption, secondOption, thirdOption);
    votePanelManager.StartVoteTimer(votingRoundTime);
    votePanelManager.ResetNamePanel(_players);
  }

  public void HandleVoteReceived(string data)
  {
    string playerID = data.Replace("\"", "");

    // remove player from dictionary
    Player player = _players[playerID];
    votePanelManager.RemoveName(player);
    SoundPlayer.PlayBloop();
  }

  public void HandleVotingRoundOver(string data)
  {
    Debug.Log("Voting round over: " + data);
    string[] splitData = data.Replace("\"", "").Split(',');
    string truth1Text = splitData[0];
    float.TryParse(splitData[1], out float truth1Percent);
    string truth2Text = splitData[2];
    float.TryParse(splitData[3], out float truth2Percent);
    string lieText = splitData[4];
    float.TryParse(splitData[5], out float liePercent);
    votePanelManager.UpdateStats(truth1Text, truth1Percent, truth2Text, truth2Percent, lieText, liePercent);
  }

  public void HandleLeaderboardStats(string data)
  {
    List<string> places = new List<string>();
    List<string> percentages = new List<string>();

    Debug.Log("Leaderboard stats: " + data);
    string[] splitData = data.Replace("\"", "").Split(',');
    Debug.Log(data.Replace("\"", ""));
    Debug.Log(splitData.Length);
    foreach (string s in splitData)
    {
      Debug.Log("{" + s + "}");
    }
    for (int i = 0; i < 6; i += 2)
    {
      if (i + 1 >= splitData.Length)
      {
        places.Add("");
        percentages.Add("");
      }
      else
      {
        string playerName = splitData[i];
        string percentageFooled = splitData[i + 1];
        places.Add(playerName);
        float.TryParse(percentageFooled, out float percentage);
        percentages.Add((percentage * 100).ToString("0") + "%");
      }
    }

    leaderboardManager.SetInfo(places[0], percentages[0], places[1], percentages[1], places[2], percentages[2]);
    leaderboardManager.ShowLeaderboard();

    SoundPlayer.PlayLeader();
  }

  public void ShowPlayerInputPanel()
  {
    playerInputManager.ShowPlayerInputPanel();
  }

  // quit game
  public void QuitGame()
  {
    Application.Quit();
  }

  // function to change to scene with name
  public void ChangeScene(string sceneName)
  {
    SceneManager.LoadScene(sceneName);
  }

  // Update is called once per frame
  void Update()
  {

  }
}