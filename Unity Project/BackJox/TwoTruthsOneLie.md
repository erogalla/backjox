Server -> Unity Host:

 - userJoined(name)

    Emitted when a user sets their name (and is ready)

 - leaderStarted()

    Emitted when the game leader clicks "Start Game"

 - recievedResponse(socket.id, truth1, truth2, lie)

    Emitted when player at socket.id returns their ttol

 - recievedVote()
    
    Emitted when a player votes

Unity host -> Server:

 - hostGame(gameType)

    Will start a game Object with basic data.

 - startResponseRound(roundTime)

    Will start the response round.

 - startVotingRound(roundTime)

    Will start 1 single voting round. Must call this multiple times

 - endGame()
    
    Will end the game and propmt server to send us the leaderboard.
