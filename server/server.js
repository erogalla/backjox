const http = require('http').createServer()
const io = require('socket.io')(http,
  {
    cors: {
      origin: "*",
    },
    allowEIO3: true
  });

const _ = require('underscore');
const utils = require('./utils');
const TwoTruthsOneLie = require('./two-truths-one-lie');
const Player = require('./player');
const ImpostorArtist = require('./Impostor Artist/impostor-artist');


/*
  gameData: {
    abcd: GameObj()
  }
*/
let games = {};

/*
  connectedPlayers: {
    123456: "abcd"
  }
*/

let connectedPlayers = {}

/*
  hosts: {
    654321: "abcd"
  }
*/
let hosts = {};

const closeGame = (hostID, game) => {
  // Tell all connected players that we're done
  io.in(game.code).emit('hostDisconnected');

  // Actually delete pointers to objectss
  delete games[game.code];
  delete hosts[hostID];
  _.values(game.players).forEach((player) => {
    delete connectedPlayers[player.id];
  })
}

io.on('connection', (socket) => {
  console.log(socket.id, 'has connected');

  // Create a game and keep track of host's id
  socket.on('hostGame', (gameType) => {
    // if we're already running a game,
    if (hosts[socket.id]) {
      closeGame(socket.id, games[hosts[socket.id]]);
    }

    const code = utils.generateCode(Object.keys(games));

    const gameData = {
      code,
      host: socket.id,
      gameType,
      players: {},
    };

    let game;
    if (gameType === 'twoTruthsOneLie') {
      game = new TwoTruthsOneLie(io, gameData);
    } else if (gameType === 'impostorArtist') {
      game = new ImpostorArtist(io, gameData);
    }

    if (game === undefined) {
      io.to(socket.id).emit('badGametype');
      return;
    }

    games[code] = game;
    console.log('Started', gameType, 'with code', code);

    // Join room
    io.to(socket.id).emit('codeCreated', code);
    socket.join(code);
    hosts[socket.id] = code;
  });

  /*
   * Handle Client Join Requests
   * Specifically, Add player to the game, and keep track if its the leader
   */
  socket.on('joinGame', (code) => {
    // Ensure valid code
    if (!games[code]) {
      io.to(socket.id).emit('badCode');
      return;
    }

    // Get game data
    const game = games[code];
    const isLeader = _.keys(game.players).length === 0;

    // Ensure game not yet started
    if (game.started) {
      io.to(socket.id).emit('gameAlreadyStarted');
      return;
    }

    // Ensure we have player spots open
    if (game.maxPlayers && _.keys(game.players).length >= game.maxPlayers) {
      io.to(socket.id).emit('gameFull');
      return;
    }

    // Add the player to the game
    game.players[socket.id] = new Player(socket);
    connectedPlayers[socket.id] = code;

    // If leader, add socket.id to gameData
    if (isLeader)
      game.leader = socket.id;

    // Join room and pass data
    socket.join(code);
    io.to(socket.id).emit('gameDetails', { gameType: game.gameType, isLeader });

    console.log(socket.id, 'joined game', game.gameType, 'in room', game.code);
  });

  // Handle any other event
  socket.onAny((eventName, ...args) => {
    if (['hostGame', 'joinGame', 'connection', 'disconnect'].includes(eventName))
      return;

    // Find game code
    const code = connectedPlayers[socket.id] || hosts[socket.id];
    if (!code) return;

    // Ensure we have a valid game
    const game = games[code];
    if (!game) return;

    // Ensure Game recognizes socket as player or as host
    const player = game.players[socket.id];
    if (!player && socket.id !== game.host) return;

    // Ensure gameObj has function
    if (game[eventName] === undefined) {
      io.to(socket.id).emit('invalidRequest');
      return;
    }

    // Call gameObj function
    game[eventName](socket, ...args);
  })

  socket.on('disconnect', () => {
    console.log(socket.id, 'has disconnected')

    const code = connectedPlayers[socket.id] || hosts[socket.id];
    if (!code) return;

    // Ensure we have a valid game
    const game = games[code];
    if (!game) return;

    // If host disconnects
    if (hosts[socket.id]) {
      // Remove pointers to game
      closeGame(socket.id, game);
    }

    // TODO: must tell someone else they're leader if leader leaves
    // If player disconnects
    if (connectedPlayers[socket.id]) {
      delete connectedPlayers[socket.id];
      if (_.keys(game.players).includes(socket.id)) {
        delete game.players[socket.id];
        io.to(game.host).emit('userDisconnected', socket.id);
        // If leader disconnects, set new player as leader
        if (socket.id === game.leader && _.keys(game.players).length !== 0) {
          const newLeader = _.values(game.players)[0];
          game.leader = newLeader.id;
          io.to(newLeader.id).emit('gameDetails', { gameType: game.gameType, isLeader: true });
        }
      }
    }
  })
});

let port = process.env.PORT;
if (port == null || port == "") {
  port = 5000;
}

io.listen(port);
