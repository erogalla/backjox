The main server will depend on a few properties for the every game object.

## Expected Game Object properties
```javascript
{
  code: string,
  gameType: string,
  started: boolean,
  host: number,
  leader: number,
  players: {
    [playerID: number]: PlayerObj()
  },
}
```

## Design decisions

> Currently, we don't allow any players to join a code once the game has started. This could be implemented on a game to game basis (if we want spectators in the future).