const _ = require('underscore');

module.exports = class TwoTruthsOneLie {
  constructor(io, gameData) {
    this.io = io;
    this.code = gameData.code;
    this.gameType = gameData.gameType;
    this.started = false;
    this.host = gameData.host;
    this.players = gameData.players;

    // TTOL Specific
    this.responses = {};
    this.responseRoundOver = false;
    this.voteRoundData = [];
    this.curVotingRound = 0;
    this.votingRoundsOver = false;
  }
  
  /* ------------------ Host ------------------ */

  startResponseRound = (socket, roundTime) => {
    // Tell host and all ready players that game has started
    console.log("this?");
    console.log(roundTime);
    this.io.in(this.code).emit('responseRoundStarted', roundTime);

    // End response round after 60 seconds
    this.responseRoundTimeout = setTimeout(() => {
      this.endResponseRound();
    }, roundTime * 1000);
  }

  
  startVotingRound = (socket, roundTime) => {
    // Ensure we have enough responses
    if (_.keys(this.responses).length === 0) return;

    // Choose response
    const randomIdx = Math.floor(Math.random() * _.keys(this.responses).length);
    const chosenID = _.keys(this.responses)[randomIdx];
    const chosenResponse = this.responses[chosenID];
    delete this.responses[chosenID];

    // Save Vote Round Details
    this.voteRoundData.push({
      submitterID: chosenID, 
      answer: chosenResponse, 
      votes: []
    });

    // Send response to all except chosen player
    if (!this.players[chosenID] || !this.players[chosenID].socket) {
      console.log('chosen player with ID', chosenID, 'not found');
      return;
    }

    // Shuffle the options
    const options = _.shuffle(_.values(chosenResponse));
    this.players[chosenID].socket.to(this.code).emit('votingRoundStarted', this.curVotingRound, this.players[chosenID].name, roundTime, options[0], options[1], options[2], chosenID);

    // Set timer
    this.votingRoundTimeout = setTimeout(() => {
      this.endVotingRound();
    }, roundTime * 1000);
  }

  endGame = (socket) => {
    this.votingRoundsOver = true;

    const leaderboard = this.voteRoundData.map((round) => {
      const playerName = this.players[round.submitterID].name;
      const foolCount = round.votes.filter((vote) => vote !== round.answer.lie).length;
      const percentageFooled = round.votes.length === 0 ? 0 : foolCount / round.votes.length;

      return { playerName, percentageFooled};
    });

    console.log(leaderboard);

    // sort leaderboard by percentageFooled in ascending order
    const sortedLeaderboard = leaderboard.sort((a, b) => {
      return b.percentageFooled - a.percentageFooled;
    });

    let leaderboardString = '';
    sortedLeaderboard.forEach((player) => {
      leaderboardString += `${player.playerName},${player.percentageFooled},`;
    });
    // Remove trailing comma
    leaderboardString = leaderboardString.slice(0, -1);

    this.io.to(this.host).emit('leaderboard', leaderboardString);
    this.io.to(this.code).emit('gameOver', true);
  }

  /* ------------------ Game ------------------ */

  endResponseRound = () => {
    // Stop Timer
    if (this.responseRoundTimeout) clearTimeout(this.responseRoundTimeout);

    // Send responseRoundOver
    this.responseRoundOver = true;
    this.io.in(this.code).emit('responseRoundOver', true);
    // this.io.to(this.host).emit('responseRoundOver', true);
  }

  endVotingRound = () => {
    // Stop Timer
    if (this.votingRoundTimeout) clearTimeout(this.votingRoundTimeout);

    const votes = this.voteRoundData[this.curVotingRound].votes;
    const answer = this.voteRoundData[this.curVotingRound].answer;

    const truth1Percentage = votes.length === 0 ? 0 
      : votes.filter((vote) => vote === answer.truth1).length / votes.length;

    const truth2Percentage = votes.length === 0 ? 0 
      : votes.filter((vote) => vote === answer.truth2).length / votes.length;

    const liePercentage = votes.length === 0 ? 0 
      : votes.filter((vote) => vote === answer.lie).length / votes.length;

    // Send votingRoundOver 
    this.io.in(this.code).emit('votingRoundOver', answer.truth1, truth1Percentage, 
                                                  answer.truth2, truth2Percentage,
                                                  answer.lie,    liePercentage);

    // Update the Voting round
    this.curVotingRound++;
  }


  /* ----------------- Client ----------------- */
  setName = (socket, name) => {
    const player = this.players[socket.id];
    player.name = name;
    player.ready = true;

    // Tell Unity Host that a user has joined
    this.io.to(this.host).emit('userJoined', name, socket.id);
  }

  startGame = (socket) => {
    // Ensure leader
    if (!this.leader || socket.id != this.leader) {
      this.io.to(socket.id).emit('notLeader');
      return;
    }

    // Start game
    this.started = true;

    // Filter players only if they're ready
    this.players = _.pick(this.players, (player) => player.ready);

    this.io.to(this.host).emit('leaderStartedGame', true);
  }

  submitResponse = (socket, response) => {
    console.log('recieved response', response);
    // Don't save response if the round is over
    if (this.responseRoundOver) return;

    // Save response
    this.responses[socket.id] = response;

    // Tell Unity host we recieved a response
    this.io.to(this.host).emit('recievedResponse', socket.id, response.truth1, response.truth2, response.lie);

    // If all responses are in, end response Round
    if (_.keys(this.responses).length === _.keys(this.players).length) {
      this.endResponseRound();
    }
  }

  vote = (socket, chosenLie, round) => {
    const player = this.players[socket.id];

    // Don't save vote if all the rounds are over
    if (this.votingRoundsOver) return;

    // Don't save vote if the round has passed
    if (round < this.curVotingRound) return;

    // Save lie
    this.voteRoundData[round].votes.push(chosenLie);

    // Tell Unity host we recieved a response
    this.io.to(this.host).emit('recievedVote', player.id);

    if (this.voteRoundData[round].votes.length === _.keys(this.players).length - 1) {
      this.endVotingRound();
    }
  }

}
