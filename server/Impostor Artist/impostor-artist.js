const _ = require('underscore');
const secretWords = require('./secret-words');

const colors = ['red', 'blue', 'green', 'orange', 'purple', 'black'];

module.exports = class ImpostorArtist{
  constructor(io, gameData) {
    this.io = io;
    this.code = gameData.code;
    this.gameType = gameData.gameType;
    this.started = false;
    this.host = gameData.host;
    this.players = gameData.players;

    // Impostor Artist Specific
    this.maxPlayers = 6;
    this.playerOrder = [];
    this.roundsStarted = false;
    this.drawingRound = 0;
    this.colorIndex = 0;
    this.lines = []
    this.startLine = true;
    this.inDrawingRound = false;
    this.votes = [];
  }

  /* ------------------ Host ------------------ */

  // Display Secret Word
  displaySecret = (socket, roundTime) => {
    this.secretWord = secretWords[Math.floor(Math.random() * secretWords.length)];

    this.impostor = _.values(this.players)[Math.floor(Math.random() * _.keys(this.players).length)];
    this.playerOrder = _.shuffle(_.values(this.players));

    this.impostor.socket.to(this.code).emit('secretWord', this.secretWord, roundTime);
    this.io.to(this.impostor.id).emit('impostor');

    // Set timer
    this.hideSecretTimer = setTimeout(() => {
      this.hideSecret();
    }, roundTime * 1000);
  }

  // Start Drawing Round (board data)
  startDrawingRound = (socket, roundTime) => {
    let nextPlayer = this.playerOrder[this.drawingRound % this.playerOrder.length];
    while (!_.values(this.players).includes(nextPlayer)) {
      this.drawingRound++;
      nextPlayer = this.playerOrder[this.drawingRound % this.playerOrder.length];
    }

    this.io.to(nextPlayer.id).emit('drawingRoundStarted', nextPlayer.color, this.lines);
    this.io.to(this.host).emit('startDrawingRound', roundTime, nextPlayer.id);
    if (!this.roundsStarted) {
      this.roundsStarted = true;
      this.io.in(this.code).emit('roundsStarted', true);
    }
    
    this.startLine = true;
    this.inDrawingRound = true;

    // Set timer
    this.drawingRoundTimeout = setTimeout(() => {
      this.endDrawingRound();
    }, roundTime * 1000);
  }

  // Start voting round
  startVotingRound = (socket, roundTime) => {
    _.values(this.players).forEach((player) => {
      const otherPlayers = _.values(this.players).filter((otherPlayer) => otherPlayer !== player);
      this.io.to(this.host).emit('votingRoundStarted', roundTime);
      this.io.to(player.id).emit('votingRoundStarted', otherPlayers.map((otherPlayer) => 
        ({ id: otherPlayer.id, name: otherPlayer.name, color: otherPlayer.color})
      ));
    })

    // Set timer
    this.votingRoundTimeout = setTimeout(() => {
      this.endVotingRound();
    }, roundTime * 1000);
  }

  // Start Guessing round (If impostor chosen)
  startGuessingRound = (socket, roundTime) => {
    
  }


  /* ------------------ Game ------------------ */

  hideSecret = () => {
    if (this.hideSecretTimer) clearTimeout(this.hideSecretTimer);
    this.io.in(this.code).emit('hideSecret', true);
  }

  endDrawingRound = () => {
    if (this.drawingRoundTimeout) clearTimeout(this.drawingRoundTimeout);
    this.drawingRound++;
    console.log(this.drawingRound, this.playerOrder.length);
    if (this.drawingRound >= this.playerOrder.length * 2) {
      this.io.in(this.code).emit('allDrawingRoundsOver', true);
    } else {
      this.io.in(this.code).emit('drawingRoundOver', true);
    }
    this.inDrawingRound = false;
  }

  endVotingRound = () =>  {
    if (this.votingRoundTimeout) clearTimeout(this.votingRoundTimeout);

    const numVotedImpostor = this.votes.filter((vote) => vote === this.impostor.id);
    
    if (numVotedImpostor.length / this.playerOrder.length >= 0.5) {
      this.io.in(this.code).emit('votedImpostor', this.impostor.id);
    } else {
      this.io.in(this.code).emit('votedWrong', true);
    }
  }


  /* ----------------- Client ----------------- */

  setName = (socket, name) => {
    const player = this.players[socket.id];
    player.name = name;
    player.ready = true;
    player.color = colors[this.colorIndex++];

    // Tell Unity Host that a user has joined
    this.io.to(this.host).emit('userJoined', name, socket.id, player.color);
  }

  startGame = (socket) => {
    // Ensure leader
    if (!this.leader || socket.id !== this.leader) {
      this.io.to(socket.id).emit('notLeader');
      return;
    }

    // Start game
    this.started = true;

    // Filter players only if they're ready
    this.players = _.pick(this.players, (player) => player.ready);

    this.io.to(this.host).emit('leaderStartedGame', true);
  }

  draw = (socket, inkLeft, x, y, endDrawing) => {
    if (endDrawing) {
      this.startLine = true;
      return;
    }

    if (inkLeft == 0 && this.inDrawingRound) {
      this.endDrawingRound();
      return;
    }

    const curColor = this.playerOrder[this.drawingRound % this.playerOrder.length].color;
    if (this.startLine) {
      this.startLine = false;
      this.lines.push([curColor])
    }
    this.lines[this.lines.length - 1].push([x, y]);

    const player = this.players[socket.id];
    this.io.to(this.host).emit('draw', inkLeft, player.id, x, 800 - y);
  }

  vote = (socket, chosenImpostor) => {
    const player = this.players[socket.id];

    // Save vote
    this.votes.push(chosenImpostor);

    // Tell Unity host we recieved a response
    this.io.to(this.host).emit('recievedVote', player.id);

    if (this.votes.length === _.keys(this.players).length) {
      this.endVotingRound();
    }
  }

}
