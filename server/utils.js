// Generate random 5 letter code
const randomCode = () => Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 4);;

// Generate random 5 letter code that doesn't exist in previousCodes
const generateCode = (previousCodes) => {
    let code = randomCode();
    while (previousCodes && previousCodes.includes(code)) code = generateCode();
    return code;
};

module.exports = { generateCode };