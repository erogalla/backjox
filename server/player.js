module.exports = class Player {
    constructor(socket) {
        this.id = socket.id;
        this.socket = socket;
        this.name = '';
        this.ready = false;
    }
}