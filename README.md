# Backjox

Final Report: https://docs.google.com/document/d/1kib2URqPSBQ89jlquyqE771dUGZ6NJ66Wrpa5YbrvlE/edit?usp=sharing


### Links
Client Website: https://backjox.games/
Unity Game: https://www.backjox.games/game
Also Unity Game: https://shanlemon.itch.io/backjox-games

### Video Trailer
https://www.youtube.com/watch?v=-IE3rTZMnxI&feature=youtu.be

### In-Class Presentation
https://www.youtube.com/watch?v=7Psj6EQRnd8

## Folder structure

`frontend/` will contain the React Client code.

`server/` will contain the Socket.io gameserver code.

`Unity Project/` will contiain the Unity Host code.

# Other Resources

Alpha Tech Doc: https://docs.google.com/document/d/1kib2URqPSBQ89jlquyqE771dUGZ6NJ66Wrpa5YbrvlE

Alpha Demo video: https://youtu.be/wZhhhBokJjk
